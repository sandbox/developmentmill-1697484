
#############################################################################
# indexer settings
#############################################################################

indexer
{
<?php if (!empty($searchd['memory_limit'])) print 'mem_limit = ' . $searchd['memory_limit'] ?>
<?php if (!empty($searchd['write_buffer'])) print 'write_buffer = ' . $searchd['write_buffer'] ?>
}

#############################################################################
# searchd settings
#############################################################################

searchd
{
listen		  = <?php print $searchd['port'] ?>
#listen		  = 9306:mysql41
log               = <?php print $searchd['log'] ?>
query_log         = <?php print $searchd['query_log'] ?>
read_timeout      = 5
client_timeout    = 300
max_children      = 30
pid_file          = <?php print $searchd['pid_file'] ?>
max_matches       = 1000
seamless_rotate   = 1
preopen_indexes   = 0
unlink_old        = 1
mva_updates_pool  = 4M
max_packet_size   = 32M
max_filters       = 256
max_filter_values = 4096
}
