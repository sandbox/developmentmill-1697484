<?php
// $Id$
/**
 * @file
 * Theme preprocess for the sphinx_advanced module.
 */

/**
 * Preprocessor for theme('searchlight_sphinx_conf').
 */
function template_preprocess_sphinx_advanced_sphinx_conf(&$vars) {
  foreach ($vars['searchd']['searchd'] as $key => $value) {
    $vars['searchd'][$key] = "$value\n";
  }
}

function template_preprocess_sphinx_advanced_sphinx_index_conf(&$vars) {
  foreach ($vars['datasource']['datasource']['conf'] as $key => $value) {
    $vars['datasource']['conf'][$key] = "$value\n";
  }
  foreach ($vars['datasource']['datasource']['index'] as $key => $value) {
    $vars['datasource']['index'][$key] = "$value\n";
  }
}

/*
 * Theme function for facet inner list
 */
function theme_sphinx_advanced_facet_list($facet_list) {
  $output = '';
  foreach ($facet_list as $facet_name => $facet) {
    $output .= '<div class="item-list">';
    $output .= '<h3>' . $facet_name . '</h3>';
    $output .= '<ul>';
    foreach ($facet as $elements) {
      $output .= '<li>';
      $output .= theme('sphinx_advanced_facet_link', $elements);
      $output .= '</li>';
    }
    $output .= '</ul>';
    $output .= '</div>';
  }
  return $output;
}

/**
 * Theme function for an individual (inactive) facet link.
 */
function theme_sphinx_advanced_facet_link($variables) {
  $class = isset($variables['class']) ? $variables['class'] : '';
  if ($class == 'active') {
    return "<div class='sphinxt-facet active'>{$variables['title']} <span class='count'>{$variables['count']}</span> <span class='remove'>{$variables['link']}</span></div>";
  }
  else {
    return "<div class='sphinx-facet'>{$variables['link']} <span class='count'>{$variables['count']}</span></div>";
  }
}