<?php
// $Id$
/**
 * @file
 * Attribute argument filter handler for Sphinx Advanced module.
 */

class sphinx_advanced_handler_argument_attribute extends views_handler_argument {
  /**
   * Override of pre_query().
   */
  function pre_query() {
    sphinx_advanced_views_init_query($this->view);
  }

  /**
   * Override of query().
   */
  function query() {
    $value = $this->argument;
    // Check if Sphinx is enabled
    $display_options = $this->view->display_handler->options;
    if ($display_options['use_sphinx']) {
      if ($this->options['sphinx_field']) {
        if ($this->argument_validated && !empty($value)) {
          $this->query->set_search_buildmode('search');
          $match = str_replace(".", "_", $this->options['sphinx_field']);
          if (!empty($this->query->datasource->fields)) {
            foreach ($this->query->datasource->fields as $info) {
              if ($info['name'] === $match) {
                  $this->query->search_filter[] = array(
                    'field' => $match,
                    'operator' => 'IN',
                    'args' => $value
                  );
                break;
              }
            }
          }
        }
      }
    }
  }

  /**
   * Override of option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();
    if ($backend = sphinx_advanced_get_backend()) {
      $options += $backend->viewsOptionDefinition($this);
    }
    $options['sphinx_field'] = array('default' => NULL);
    return $options;
  }

  /**
   * Override of options_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    if ($backend = sphinx_advanced_get_backend()) {
      $backend->viewsOptionsForm($form, $form_state, $this);
    }
    // Check if Sphinx is enabled
    $display_options = $this->view->display_handler->options;
    if ($display_options['use_sphinx']) {
      $index_name = $display_options['sphinx_index'];
      // Get index fields
      $datasource = sphinx_advanced_datasource_load($index_name);
      if ($datasource->fields) {
        foreach ($datasource->fields as $key => $field) {
          $options[$field['name']] = $field['name'];
        }
        $form['sphinx_field'] = array(
          '#type' => 'select',
          '#multiple' => FALSE,
          '#title' => t('Sphinx Attribute to be used with this argument'),
          '#options' => $options,
          '#description' => t('Select attribute to be used as argument.'),
          '#default_value' => $this->options['sphinx_field'],
        );
      }
    }
  }
}
