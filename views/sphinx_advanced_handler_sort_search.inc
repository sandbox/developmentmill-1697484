<?php
// $Id$
/**
 * @file
 * Sort handler for Sphinx Advanced module.
 */

class sphinx_advanced_handler_sort_search extends views_handler_sort {
  /**
   * Override of query().
   */
  function query() {
    $this->query->add_orderby(NULL, 'sphinx_weight', $this->options['order'], 'sphinx_weight');
    if (isset($this->query->fields['sphinx_weight'])) {
      unset($this->query->fields['sphinx_weight']);

      foreach ($this->query->orderby as $key => $info) {
        if ($info['field'] == 'sphinx_weight') {
          unset($this->query->orderby[$key]);
        }
      }
    }
  }
}
