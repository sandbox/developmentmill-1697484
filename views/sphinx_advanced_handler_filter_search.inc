<?php
// $Id$
/**
 * @file
 * Search filter handler for Sphinx Advanced module.
 */

class sphinx_advanced_handler_filter_search extends views_handler_filter {
  /**
   * Override of pre_query().
   */
  function pre_query() {
    sphinx_advanced_views_init_query($this->view);
  }

  /**
   * Override of query().
   */
  function query() {
    // TODO: check and fix queries to Sphinx
    if (!empty($this->query->sphinx)) {
      $value = is_array($this->value) ? reset($this->value) : $this->value;
      $value = trim($value);
      // There is a search query.
      if (!empty($value)) {
        $this->query->set_search_buildmode('search');
      }
      // There is no search query.
      else {
        // If we are replacing a core search filter the 'operator' option may be
        // set. Use this to determine buildmode.
        if (!empty($this->options['operator']) && in_array($this->options['operator'], array('required', 'optional'))) {
          if ($this->options['operator'] === 'required') {
            $this->query->set_search_buildmode('empty');
          }
        }
        else {
          $this->query->set_search_buildmode('default');
        }
      }
      // Check, if search should be exected only on given fields
      if ($this->options['sphinx_context_fields']) {
        foreach ($this->options['sphinx_context_fields'] as $context_field) {
          if (!empty($this->query->datasource->context_fields)) {
            if (in_array($context_field, $this->query->datasource->context_fields)) {
              $this->query->search_fields[] = trim($context_field);
            }
          }
        }
      }
      $this->query->set_search_options($this->options);
      $this->query->set_search_query($value);
    }
  }

  /**
   * Override of option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();
    if ($backend = sphinx_advanced_get_backend()) {
      $options += $backend->viewsOptionDefinition($this);
    }
    $options['sphinx_context_fields'] = array('default' => NULL);
    return $options;
  }

  /**
   * Override of options_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    if ($backend = sphinx_advanced_get_backend()) {
      $backend->viewsOptionsForm($form, $form_state, $this);
    }
    // Check if Sphinx is enabled
    $display_options = $this->view->display_handler->options;
    if ($display_options['use_sphinx']) {
      $index_name = $display_options['sphinx_index'];
      // Get index context fields
      $datasource = sphinx_advanced_datasource_load($index_name);
      if ($datasource->context_fields) {
        foreach ($datasource->context_fields as $key => $context_field) {
          $options[$context_field] = $context_field;
        }
        $form['sphinx_context_fields'] = array(
          '#type' => 'select',
          '#multiple' => TRUE,
          '#title' => t('Sphinx Context fields'),
          '#options' => $options,
          '#description' => t('Select context fields to be used in search.'),
          '#default_value' => $this->options['sphinx_context_fields'],
        );
      }
    }
  }

  /**
   * Provide a textfield for search query.
   */
  function value_form(&$form, &$form_state) {
    if ($backend = sphinx_advanced_get_backend()) {
      $backend->viewsValueForm($form, $form_state, $this);
    }
  }

  function is_exposed() {
    return TRUE;
  }
}
