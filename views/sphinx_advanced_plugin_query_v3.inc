<?php
// $Id$
/**
 * @file
 * Query handler for Sphinx Advanced module.
 */

class sphinx_advanced_plugin_query extends views_plugin_query_default {
  var $backend;
  var $client;
  var $datasource;
  var $datasource_id;
  var $search_buildmode;
  var $search_facets;
  var $search_filter;
  var $search_fields;
  var $search_query_extended;
  var $search_options;
  var $search_result;
  var $search_sort;
  var $search_query;
  var $sphinx;

  /**
   * Override of init().
   */
  function init($base_table = 'node', $base_field = 'nid', $options = array(), $backend) {
    parent::init($base_table, $base_field, $options);
    // Init the fields array on behalf of the parent.
    $this->fields = isset($this->fields) ? $this->fields : array();

    $this->datasource = sphinx_advanced_datasource_load($backend->index_name);
    // Use main index, not with delta index
    if ($this->datasource->parent_index) {
      $this->datasource = sphinx_advanced_datasource_load($this->datasource->parent_index);
    }

    $this->datasource_id = $this->datasource->machine_name;
    $this->backend = $backend;
    $this->client = $backend->initClient();

    $this->offset = NULL;
    $this->limit = NULL;
    $this->search_buildmode = NULL;
    $this->search_facets = array();
    $this->search_filter = array();
    $this->search_fields = array();
    $this->search_options = array();
    $this->search_result = array();
    $this->search_sort = array();
    $this->search_query_extended = array();
    $this->search_query = '';

    // This flag lets others know we are a Sphinx-based view.
    $this->sphinx = TRUE;
  }

  /**
   * Add order criteria.
   */
  function add_orderby($table, $field, $order, $alias = '', $params = array()) {
    if ($field === 'sphinx_weight') {
      $this->search_sort[] = array(
        'field' => 'sphinx_weight',
        'direction' => $order,
      );
    }
    else {
      // Call the parent method to add the orderby SQL side.
      parent::add_orderby($table, $field, $order, $alias, $params);
    }
  }

  /**
   * Add search facet.
   */
  function add_search_facet($name, $limit = 5, $options = array()) {
    $this->search_facets[] = array(
      'field' => $name,
      'limit' => $limit,
    ) + $options;
  }

  /**
   * Set the Search build mode behavior.
   *
   * @param string $mode
   *   One of the following modes:
   *   'default': Build the Search for facets, etc. but use the View's default
   *              result set.
   *   'search':  Build the Search for facets, etc. and use the search backend
   *              to generate the result set.
   *   'empty':   Build the Search for facets, etc. but blank this View's
   *              result set.
   * @param boolean $force
   *   Force the mode specified. Otherwise, will only set the build mode if no
   *   other mode has been set yet.
   */
  function set_search_buildmode($mode = 'default', $force = FALSE) {
    if (!isset($this->search_buildmode) || $force) {
      $this->search_buildmode = $mode;
    }
  }

  /**
   * Set search options.
   */
  function set_search_options($options) {
    $this->search_options = $options;
  }

  /**
   * Set search query.
   */
  function set_search_query($query) {
    $this->search_query = $query;
  }

  /**
   * Get search facet.
   */
  function get_search_facet($facet) {
    return isset($this->search_result_facets[$facet]) ? $this->search_result_facets[$facet] : array();
  }

  /**
   * Get view args.
   */
  protected function get_args($args, $offset = 2) {
    $args = array_slice($args, $offset);
    if (count($args) == 1 && is_array(reset($args))) {
      return current($args);
    }
    return $args;
  }

  /**
   * Builds the necessary info to execute the query.
   */
  function build(&$view) {
    $this->set_search_buildmode();

    // Fail the rest of the build entirely if the hideEmpty option is true and
    // there is no search query available. We don't exit right here in order
    // to allow facet queries to run.
    if (empty($this->fields) || $this->search_buildmode === 'empty') {
      parent::add_where(0, "FALSE");
    }

    //
    // Start: same as parent.
    //
    // Make the query distinct if the option was set.
    if (!empty($this->options['distinct'])) {
      $this->set_distinct();
    }

    // Store the view in the object to be able to use it later.
    $this->view = $view;

    $view->init_pager();

    // Let the pager modify the query to add limits.
    $this->pager->query();
    //
    // End: same as parent.
    //
    // Get filters options from View and check, if it can be used in Sphinx
    //print kpr($this);
//    print kpr($view->argument);
    if ($view->filter) {
      $this->process_filters($view, $this);
    }

    if ($this->view->argument) {
      $this->process_argument($view, $this);
    }

    // Get sort options from View and check, if it can be used in Sphinx
    if ($view->sort) {
      unset($this->search_sort);
      $this->process_sort($view, $this);
    }
    // Views query token replacements.
    $replacements = module_invoke_all('views_query_substitutions', $view);
    // Do token replacement against filter fields & args.
    if (!empty($replacements)) {
      foreach ($this->search_filter as $j => $filter) {
        $this->search_filter[$j]['field'] = str_replace(array_keys($replacements), $replacements, $this->search_filter[$j]['field']);
        if (is_array($this->search_filter[$j]['args'])) {
          foreach ($this->search_filter[$j]['args'] as $k => $arg) {
            $this->search_filter[$j]['args'][$k] = str_replace(array_keys($replacements), $replacements, $arg);
          }
        }
        else {
          $this->search_filter[$j]['args'] = str_replace(array_keys($replacements), $replacements, $this->search_filter[$j]['args']);
        }
      }
    }
    // Set backend client options.
    $this->backend->setOptions($this->client, $this->datasource, $this->search_options);
    $this->backend->setSort($this->client, $this->datasource, $this->search_sort);
    $this->backend->setFilter($this->client, $this->datasource, $this->search_filter);
    // $this->backend->setPager($this->client, $this->offset, $this->limit);
    // Main query execution.
    if (!empty($this->search_query)) {
      if (!empty($this->search_fields)) {
        $this->client->SetMatchMode(SPH_MATCH_EXTENDED);
        $this->search_query = '@(' . implode(',', $this->search_fields) . ') *' . $this->backend->EscapeSphinxQL($this->search_query) . '*';
      }
      else {
        $this->search_query = '*' . $this->backend->EscapeSphinxQL($this->search_query) . '*';
      }
    }
    // Extended query condition
    if (!empty($this->search_query_extended)) {
      $this->client->SetMatchMode(SPH_MATCH_EXTENDED);
      foreach ($this->search_query_extended as $extended) {
        // Add extended conditions for given field
        $this->search_query .= ' ' . '@' . $extended['field'] . ' *' . $this->backend->EscapeSphinxQL($extended['args']) . '*';
      }
    }
    $this->search_result = $this->backend->executeQuery($this->client, $this->datasource, $this->search_query);
    // Build facets.
    $this->search_result_facets = $this->backend->facetBuild($this->client, $this->datasource, $this->search_query, $this->search_facets);
//    print kpr($this->view);
//    print kpr($this);
    if ($this->search_result_facets || $this->view->exposed_input) {
      $this->process_facets($this);
    }

    if ($this->search_buildmode === 'search' || $this->search_buildmode == 'default') {
      if ($this->search_result) {
        $replace = array_fill(0, sizeof($this->search_result['result']), '%d');
        $in = '(' . implode(", ", $replace) . ')';
        // We use array_values() because the checkboxes keep keys and that can cause
        // array addition problems.
        parent::add_where(0, "{$this->base_table}.{$this->base_field} IN $in", array_values($this->search_result['result']));
      }
      else {
        $this->search_buildmode = 'empty';
      }
    }
    //
    // Start: same as parent.
    //
    $this->query = $this->query();
    if (empty($this->options['disable_sql_rewrite'])) {
      $this->final_query = db_rewrite_sql($this->query, $view->base_table, $view->base_field, array('view' => $view));
      $this->count_query = db_rewrite_sql($this->query(TRUE), $view->base_table, $view->base_field, array('view' => $view));;
    }
    else {
      $this->final_query = $this->query;
      $this->count_query = $this->query(TRUE);
    }
    $this->query_args = $this->get_where_args();
    //
    // End: same as parent.
    //
  }

  /**
   * Executes the query and fills the associated view object with according
   * values.
   *
   * Values to set: $view->result, $view->total_rows, $view->execute_time,
   * $view->current_page.
   */
  function execute(&$view) {
    if ($this->search_buildmode === 'empty') {
      //
    }
    elseif ($this->search_buildmode === 'default' || $this->search_buildmode === 'search') {
      parent::execute($view);
    }
//    elseif ($this->search_buildmode === 'search') {
//      // Store values prior to execute().
//      $offset = $this->offset;
//      $limit = $this->limit;
//      $current_page = $this->pager->current_page;
//      $this->offset = $this->limit = 0;
//      parent::execute($view);
//
//      if (!empty($this->search_result)) {
//        //error_log(var_export($this->search_result['total'], TRUE), 3, 'placeholder');
//        //error_log(var_export($view->total_rows, TRUE), 3, 'placeholder');
//        $this->pager->set_current_page($current_page);
//        $this->pager->update_page_info();
//      }
//
//      // Restore original values.
//      $this->offset = $offset;
//      $this->limit = $limit;
//
//      if ($view->built && !empty($this->search_result)) {
//        // Ensure the order of the result set from Views matches that of the
//        // search backend. Don't attempt to do this for aggregate queries.
//        if (empty($this->has_aggregate)) {
//          $sortmap = array();
//          $positions = array_flip(array_values($this->search_result['result']));
//          $i = count($positions);
//          foreach ($view->result as $num => $row) {
//            $key = $row->{$view->base_field};
//            // If in search results use its position in the resultset.
//            if (isset($positions[$key])) {
//              $sortmap[$num] = $positions[$key];
//            }
//            // If not, move to the end of the stack.
//            else {
//              $sortmap[$num] = $i;
//              $i++;
//            }
//          }
//          array_multisort($sortmap, $view->result);
//        }
//      }
//    }
  }

  /**
   * Collect existing Sphinx filters from View filters
   */
  function process_filters($view, $query) {
    foreach ($view->filter as $filter) {
      // Don't check Sphinx filters
      if ($filter->table != 'sphinx_advanced') {
        $match = str_replace(".", "_", $filter->field);
        if (!empty($query->datasource->fields)) {
          // If it is CCK date filter
          if ($match == 'date_filter') {
            foreach ($filter->query_fields as $field) {
              $match = str_replace(".", "_", $field['field']['field_name']);
              foreach ($query->datasource->fields as $info) {
                if ($info['name'] === $match) {
                  if ($info['type'] == 'timestamp') {
                    $filter->value['min'] = isset($filter->value['min']) ? strtotime($filter->value['min']) : 0;
                    $filter->value['max'] = isset($filter->value['max']) ? strtotime($filter->value['max']) : time();
                    $filter->value['value'] = isset($filter->value['value']) ? strtotime($filter->value['value']) : 0;
                  }

                  if ($filter->value['min'] != 0 || $filter->value['max'] != 0 || $filter->value['value'] != 0)
                  $query->search_filter[] = array(
                    'field' => $match,
                    'operator' => !empty($filter->operator) ? strtoupper($filter->operator) : 'IN',
                    'args' => $filter->value
                  );
                }
              }
            }
          }
          else {
            // Drupal date filter
            foreach ($query->datasource->fields as $info) {
              if ($info['name'] === $match) {
                if ($info['type'] == 'md5') {
                  if (is_array($filter->value)) {
                    foreach ($filter->value as $key => $value) $filter->value[$key] = md5($value);
                  }
                  else $filter->value = md5($filter->value);
                }

                if ($info['type'] == 'timestamp') {
                  if (isset($filter->value['min'])) $filter->value['min'] = strtotime($filter->value['min']);
                  if (isset($filter->value['max'])) $filter->value['max'] = strtotime($filter->value['max']);
                  if (isset($filter->value['value'])) $filter->value['value'] = strtotime($filter->value['value']);
                }

                $query->search_filter[] = array(
                  'field' => $match,
                  'operator' => !empty($filter->operator) ? strtoupper($filter->operator) : 'IN',
                  'args' => $filter->value
                );
              }
            }
          }
        }
      }
    }
  }

  /**
   * Collect existing Sphinx sort criterias from View sort criterias
   */
  function process_sort($view, $query) {
    foreach ($view->sort as $key => $sort) {
    // Don't check Sphinx sort
      if ($sort->table === 'sphinx_advanced') {
        $query->search_sort[] = array(
          'field' => 'sphinx_weight',
          'direction' => $sort->options['order'],
        );
        // unset($this->orderby);
        unset($view->sort[$key]);
      }
      else {
        $match = str_replace(".", "_", $sort->field);
        if (!empty($query->datasource->fields)) {
          foreach ($query->datasource->fields as $info) {
            if ($info['name'] === $match) {
              $query->search_sort[] = array(
                'field' => $info['name'],
                'direction' => $sort->options['order'],
              );
              break;
            }
          }
        }
      }
    }
  }

  /**
   * Collect existing Sphinx filters from View arguments
   */
  function process_argument($view, $query) {
    foreach ($view->argument as $key => $arg) {
      // Don't check Sphinx args
      if ($arg->table != 'sphinx_advanced') {
        $match = str_replace(".", "_", $arg->field);
        if (!empty($query->datasource->fields)) {
          foreach ($query->datasource->fields as $info) {
            if ($info['name'] === $match) {
                $query->search_filter[] = array(
                  'field' => $match,
                  'operator' => 'IN',
                  'args' => $arg->value
                );
              break;
            }
          }
        }
      }
    }
  }

  /**
   * Process Sphinx facets
   */
  function process_facets($query) {
    global $_sphinx_advanced_facets;
    $_sphinx_advanced_facets = array();
    if ($query->view->filter['facets']->options['sphinx_fields']) {
      // Process selected facets
      foreach ($query->view->exposed_input as $input_name => $input) {
        // check if it is facet input
        $fields_list = array_keys($query->view->filter['facets']->options['sphinx_fields']);
        if (in_array($input_name, $fields_list) && !empty($input)) {
          $_sphinx_advanced_facets[$input_name]['type'] = $query->datasource->fields[$input_name]['type'];
          $_sphinx_advanced_facets[$input_name]['facet_title'] = $query->datasource->fields[$input_name]['title'];
          if (!is_array($input)) $input = array($input);
          // Prepare values
          foreach ($input as $input_value) {
            $_sphinx_advanced_facets[$input_name]['values'][$input_value]['active'] = TRUE;
            $_sphinx_advanced_facets[$input_name]['values'][$input_value]['id'] = $input_value;
            $_sphinx_advanced_facets[$input_name]['values'][$input_value]['count'] = (!empty($query->search_result['total'])) ? $query->search_result['total'] : 0;
            $input_ordinal = NULL;
            // Get Drupal value from ordinal for sql_attr_str2ordinal and sql_attr_string
            if ($_sphinx_advanced_facets[$input_name]['type'] == 'text' || $_sphinx_advanced_facets[$input_name]['type'] == 'string') {
              if (isset($query->backend->ordinals[$query->datasource->machine_name][$input_name])) {
                $input_ordinal = array_search($input_value, $query->backend->ordinals[$query->datasource->machine_name][$input_name]);
              }
              else {
                $input_ordinal = $query->backend->sphinxGetOrdinal($query->datasource, $input_name, $input_value);
              }
            }

            if (empty($input_ordinal)) $input_ordinal = $input_value;
            // Get rendered value
            $input_output = $query->backend->sphinxGetRenderer($query->datasource, $input_name, $input_ordinal);
            $_sphinx_advanced_facets[$input_name]['values'][$input_value]['title'] = $input_output;
          }
        }
      }
      // Prepare possible facets list
      foreach ($query->search_result_facets as $facet_name => $facet) {
        $_sphinx_advanced_facets[$facet_name]['type'] = $query->datasource->fields[$facet_name]['type'];
        $_sphinx_advanced_facets[$facet_name]['facet_title'] = $query->datasource->fields[$facet_name]['title'];
        $_sphinx_advanced_facets[$facet_name]['context_field'] = $query->datasource->fields[$facet_name]['context'];

        // It is active facet field
        foreach ($facet as $value) {
          $_sphinx_advanced_facets[$facet_name]['values'][$value['id']]['id'] = $value['id'];
          $_sphinx_advanced_facets[$facet_name]['values'][$value['id']]['count'] = $value['count'];

          $ordinal = NULL;
          // Get Drupal value from ordinal for sql_attr_str2ordinal and sql_attr_string
          if ($_sphinx_advanced_facets[$input_name]['type'] == 'text' || $_sphinx_advanced_facets[$input_name]['type'] == 'string') {
            if (isset($query->backend->ordinals[$query->datasource->machine_name][$facet_name])) {
              $ordinal = array_search($value['id'], $query->backend->ordinals[$query->datasource->machine_name][$facet_name]);
            }
            else {
              $ordinal = $query->backend->sphinxGetOrdinal($query->datasource, $facet_name, $value['id']);
            }
          }
          if (empty($ordinal)) $ordinal = $value['id'];
          // Get rendered value
          $output = $query->backend->sphinxGetRenderer($query->datasource, $facet_name, $ordinal);
          $_sphinx_advanced_facets[$facet_name]['values'][$value['id']]['title'] = $output;
        }
      }
    }
  }
}

