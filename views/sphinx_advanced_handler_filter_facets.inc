<?php
// $Id$
/**
 * @file
 * Facet filter handler for Sphinx Advanced module.
 */

class sphinx_advanced_handler_filter_facets extends views_handler_filter {
  /**
   * Override of pre_query().
   */
  function pre_query() {
    sphinx_advanced_views_init_query($this->view);
  }

  /**
   * Override of option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['sphinx_fields'] = array('default' => NULL);
    $options['items'] = array('default' => 5);
    return $options;
  }

  /**
   * Override of options_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    // Check if Sphinx is enabled
    $display_options = $this->view->display_handler->options;
    if ($display_options['use_sphinx']) {
      $index_name = $display_options['sphinx_index'];
      // Get index fields
      $datasource = sphinx_advanced_datasource_load($index_name);
      if ($datasource->fields) {
        foreach ($datasource->fields as $key => $field) {
          $options[$field['name']] = $field['name'];
        }

        $form['sphinx_fields'] = array(
          '#type' => 'select',
          '#multiple' => TRUE,
          '#title' => t('Index Fields'),
          '#options' => $options,
          '#description' => t('Select index fields to use.'),
          '#default_value' => $this->options['sphinx_fields'],
        );

        $form['items'] = array(
          '#type' => 'textfield',
          '#title' => t('Facet limit'),
          '#description' => t('Limit numbers of facet values'),
          '#default_value' => $this->options['items'],
        );
      }
    }
  }

  /**
   * Override of query().
   */
  function query() {
    // Check if Sphinx is enabled
    $display_options = $this->view->display_handler->options;
    if ($display_options['use_sphinx']) {
      foreach ($this->view->exposed_input as $facet_name => $facet_value) {
        // If it is Sphinx facet
        if (in_array($facet_name, $this->options['sphinx_fields'])) {
          if (is_array($facet_value)) {
            foreach ($facet_value as $value) {
              // Use AND operator for multi-values
              $this->query->search_filter[] = array(
                'field' => $facet_name,
                'operator' => '=',
                'args' => $value
              );
            }
          }
          elseif (!empty($facet_value)) {
            $this->query->search_filter[] = array(
              'field' => $facet_name,
              'operator' => 'IN',
              'args' => $facet_value
            );
          }
        }
        // Check if it is context field
        else {
          foreach ($this->query->datasource->fields as $field) {
            if ($field['context'] && $facet_name == $field['context'] && in_array($field['name'], $this->options['sphinx_fields']) && !empty($facet_value)) {
              $this->query->search_query_extended[] = array(
              'field' => $facet_name,
              'args' => $facet_value
            );
            }
          }
        }
      }
    }

    if ($this->options['sphinx_fields']) {
      foreach ($this->options['sphinx_fields'] as $field) {
        // Add search facet
        // Add this facet to be built by the backend.
        $limit = isset($this->options['items']) ? $this->options['items'] : 10;
        $this->query->add_search_facet($field, $limit);
      }
    }
  }

  function can_expose() {
    return FALSE;
  }
}
