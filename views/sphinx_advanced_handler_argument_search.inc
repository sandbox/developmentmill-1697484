<?php
// $Id$
/**
 * @file
 * Search argument filter handler for Sphinx Advanced module.
 */

class sphinx_advanced_handler_argument_search extends views_handler_argument {
  /**
   * Override of pre_query().
   */
  function pre_query() {
    sphinx_advanced_views_init_query($this->view);
  }

  /**
   * Override of query().
   */
  function query() {
    $value = $this->argument;
    // Check if Sphinx is enabled
    $display_options = $this->view->display_handler->options;
    if ($display_options['use_sphinx']) {
      $value = is_array($value) ? reset($value) : $value;
      $value = trim($value);
      // Check, if search should be exected only on given fields
      if ($this->options['sphinx_context_fields']) {
        foreach ($this->options['sphinx_context_fields'] as $context_field) {
          if (!empty($this->query->datasource->context_fields)) {
            if (in_array($context_field, $this->query->datasource->context_fields)) {
              $this->query->search_fields[] = trim($context_field);
            }
          }
        }
      }

      if ($this->argument_validated && !empty($value)) {
        $this->query->set_search_buildmode('search');
        $this->query->set_search_options($this->options);
        $this->query->set_search_query($value);
      }
    }
  }

  /**
   * Override of option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();
    if ($backend = sphinx_advanced_get_backend()) {
      $options += $backend->viewsOptionDefinition($this);
    }
    $options['sphinx_context_fields'] = array('default' => NULL);
    return $options;
  }

  /**
   * Override of options_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    if ($backend = sphinx_advanced_get_backend()) {
      $backend->viewsOptionsForm($form, $form_state, $this);
    }
    // Check if Sphinx is enabled
    $display_options = $this->view->display_handler->options;
    if ($display_options['use_sphinx']) {
      $index_name = $display_options['sphinx_index'];
      // Get index context fields
      $datasource = sphinx_advanced_datasource_load($index_name);
      if ($datasource->context_fields) {
        foreach ($datasource->context_fields as $key => $context_field) {
          $options[$context_field] = $context_field;
        }
        $form['sphinx_context_fields'] = array(
          '#type' => 'select',
          '#multiple' => TRUE,
          '#title' => t('Sphinx Context fields'),
          '#options' => $options,
          '#description' => t('Select context fields to be used in search.'),
          '#default_value' => $this->options['sphinx_context_fields'],
        );
      }
    }
  }
}
