<?php
// $Id$
/**
 * @file
 * The advanced display plugin handler. Display Sphinx options
 */

class sphinx_advanced_plugin_display_extender extends views_plugin_display {
  function __construct(&$view, &$display) {
    $this->view = $view;
    $this->display = $display;
  }

  /**
   * Static member function to list which sections are defaultable
   * and what items each section contains.
   */
  function defaultable_sections(&$sections, $section = NULL) {
    $sections['use_sphinx'] = array(
      'use_sphinx' => array('use_sphinx', 'sphinx_index'),
    );
  }

  function option_definition(&$options) {
    // Set this option as global for all displays
    // $options['defaults']['default']['use_sphinx'] = array(TRUE);
    $options['use_sphinx'] = array(
      'default' => FALSE,
      'bool' => TRUE,
    );
    $options['sphinx_index'] = array(
      'default' => NULL,
    );
  }

    /**
   * Provide the default summary for options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    $categories['sphinx'] = array(
      'title' => t('Sphinx settings'),
    );
    $options['use_sphinx'] = array(
      'category' => 'sphinx',
      'title' => t('Use Sphinx'),
      'value' => $this->display->get_option('use_sphinx') ? t('Yes') : t('No'),
      'desc' => t('Change whether or not this display will use Sphinx.'),
    );
  }

  /**
   * Provide the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    switch ($form_state['section']) {
      case 'use_sphinx':
        $form['#title'] = t('Use Sphinx when available to load this view');
        $form['description'] = array(
          '#prefix' => '<div class="description form-item">',
          '#suffix' => '</div>',
          '#value' => t('If set, this view will use an Sphinx search for view, table sorting and exposed filters.'),
        );
        $form['use_sphinx'] = array(
          '#type' => 'checkbox',
          '#title' => 'Use Sphinx to this view.',
          '#default_value' => $this->display->get_option('use_sphinx'),
        );

        // Index options
        foreach (sphinx_advanced_datasource_load(NULL) as $key => $item) {
          $options[$key] = $item->name;
        }
        ksort($options);
        $form['sphinx_index'] = array(
          '#type' => 'select',
          '#title' => t('Search Index'),
          '#options' => $options,
          '#description' => t('Select search index to use.'),
          '#default_value' => $this->display->get_option('sphinx_index'),
          '#dependency' => array(
            'edit-use-sphinx' => array(TRUE),
          ),
          '#process' => array('views_process_dependency'),
        );
        break;

    }
  }

    /**
   * Perform any necessary changes to the form values prior to storage.
   * There is no need for this function to actually store the data.
   */
  function options_submit($form, &$form_state) {
    $section = $form_state['section'];
    switch ($section) {
      case 'use_sphinx':
        $this->display->set_option($section, (bool)$form_state['values'][$section]);
        $this->display->set_option('sphinx_index', $form_state['values']['sphinx_index']);
    }
  }

  /**
   * Set up any variables on the view prior to execution.
   */
  function pre_execute() { }

  /**
   * Inject anything into the query that the display_extender handler needs.
   */
  function query() { }
}
