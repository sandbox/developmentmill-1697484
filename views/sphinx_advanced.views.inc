<?php
// $Id$
/**
 * @file
 * View implementation for Sphinx Advanced module.
 */

/**
 * Implements hook_views_plugins().
 */
function sphinx_advanced_views_plugins() {
  return array(
    'module' => 'sphinx_advanced',
    'display_extender' => array(
      'sphinx_advanced' => array(
        'title' => 'Sphinx Display Extender',
        'file' => 'sphinx_advanced_plugin_display_extender.inc',
        'path' => drupal_get_path('module', 'sphinx_advanced') . '/views',
        'handler' => 'sphinx_advanced_plugin_display_extender',
      ),
    ),
    'query' => array(
      'sphinx_advanced' => array(
        'file' => 'sphinx_advanced_plugin_query_v3.inc',
        'path' => drupal_get_path('module', 'sphinx_advanced') . '/views',
        'title' => t('Sphinx advanced'),
        'help' => t('Sphinx query plugin.'),
        'handler' => 'sphinx_advanced_plugin_query',
        'parent' => 'views_query',
      ),
    ),
  );
}

/**
 * Implements hook_views_handlers().
 */
function sphinx_advanced_views_handlers() {
  return array(
    'info' => array('path' => drupal_get_path('module', 'sphinx_advanced') . '/views'),
    'handlers' => array(
      'sphinx_advanced_handler_argument_search' => array('parent' => 'views_handler_argument'),
      'sphinx_advanced_handler_argument_attribute' => array('parent' => 'views_handler_argument'),
      'sphinx_advanced_handler_filter_search' => array('parent' => 'views_handler_filter'),
      'sphinx_advanced_handler_filter_facets' => array('parent' => 'views_handler_filter'),
      'sphinx_advanced_handler_sort_search' => array('parent' => 'views_handler_sort'),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function sphinx_advanced_views_data() {
  $data = array();
  $data['sphinx_advanced']['table']['group'] = t('Search');
  $data['sphinx_advanced']['table']['join'] = array(
    '#global' => array(),
  );
  $data['sphinx_advanced']['search'] = array(
    'title' => t('Sphinx Advanced'),
    'help' => t('Filter results by a Sphinx search.'),
    'argument' => array('handler' => 'sphinx_advanced_handler_argument_search'),
    'filter' => array('handler' => 'sphinx_advanced_handler_filter_search'),
    'sort' => array('handler' => 'sphinx_advanced_handler_sort_search'),
  );
  $data['sphinx_advanced']['attributes'] = array(
    'title' => t('Sphinx Advanced attribute'),
    'help' => t('Use argument as a Sphinx attribute.'),
    'argument' => array('handler' => 'sphinx_advanced_handler_argument_attribute'),
  );
  $data['sphinx_advanced']['facets'] = array(
    'title' => t('Sphinx facets'),
    'help' => t('Filter results by sphinx facets.'),
    'filter' => array('handler' => 'sphinx_advanced_handler_filter_facets'),
  );
  return $data;
}

/**
 * Init (or more accurately "hijack") the Views query plugin. This is called
 * from the pre_query() method of the searchlight filter, argument handlers.
 */
function sphinx_advanced_views_init_query(&$view) {
  // The following is a copy of the $view->query_init() method, with the only
  // difference being that we use the sphinx backend plugin rather than
  // the default views query plugin and pass the backend to the query plugin.
  if (!empty($view->query)) {
    $class = get_class($view->query);
    if ($class && $class != 'stdClass' && !empty($view->query->sphinx)) {
      // return if query is already initialized.
      return;
    }
  }

  // Create and initialize the query object. If no backend is present, leave
  // the view alone.
  if ($backend = sphinx_advanced_get_backend()) {
    $views_data = views_fetch_data($view->base_table);
    $view->base_field = $views_data['table']['base']['field'];
    if (!empty($views_data['table']['base']['database'])) {
      $view->base_database = $views_data['table']['base']['database'];
    }

    // Check that an active datasource can be found for this base table.
    $display_options = $view->display_handler->options;
    if ($display_options['use_sphinx']) {
      $backend->index_name = $display_options['sphinx_index'];
      $query_options = $view->display_handler->get_option('query');
      $view->query = sphinx_advanced_get_query($view->base_table, $view->base_field, $query_options['options'], $backend);
    }
  }
}