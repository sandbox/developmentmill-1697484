<?php
// $Id$
define('SPHINX_FILTER_MAX', 100000000000);
define('SPHINX_FILTER_MIN', 0);
/**
 * @file
 * Sphinx backend for integration with Drupal
 */
class BackendSphinx {
  var $id;
  var $settings;

  /**
   * Set default settings.
   */
  function settingsDefault() {
    $this->settings = array(
      'port' => 9312,
      'memory_limit' => '512M',
      'write_buffer' => '32M',
      'index_ttl' => 0,
      'sql_sock' => '',
      'index' => array(
        'morphology' => 'stem_en',
        'charset_table' => NULL,
        'ngram_len' => NULL,
        'ngram_chars' => NULL,
        'charset_type' => 'utf-8',
        'html_strip' => 0,
        'enable_star' => 1,
        'min_prefix_len' => 0,
        'min_infix_len' => 1,
      ),
    );
  }

  /**
   * Include the Sphinx PHP API library.
   */
  function sphinxIncludeClient() {
    module_load_include('php', 'sphinx_advanced', 'includes/sphinxapi');
  }

  /**
   * Backend callback for filter handler $handler->value_form() method.
   */
  function viewsValueForm(&$form, &$form_state, &$handler) {
    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#size' => 30,
      '#default_value' => $handler->value,
    );
    if (isset($handler->options['expose']['identifier'])) {
      $identifier = $handler->options['expose']['identifier'];
      if (!empty($form_state['exposed']) && !isset($form_state['input'][$identifier])) {
        $form_state['input'][$identifier] = $handler->value;
      }
    }
  }


  /**
   * Override of viewsOptionDefinition().
   */
  function viewsOptionDefinition(&$handler) {
    $this->sphinxIncludeClient();
    $options['hideEmpty'] = array('default' => TRUE);
    $options['matchMode'] = array('default' => SPH_MATCH_ALL);
    return $options;
  }

  /**
   * Override of viewsOptionsForm().
   */
  function viewsOptionsForm(&$form, &$form_state, &$handler) {
    $this->sphinxIncludeClient();

    $form['hideEmpty'] = array(
      '#title' => t('Hide all results when empty'),
      '#type' => 'checkbox',
      '#default_value' => $handler->options['hideEmpty'],
    );

    $form['matchMode'] = array(
      '#title' => t('Search mode'),
      '#type' => 'select',
      '#options' => array(
        SPH_MATCH_ALL => t('Match all query words'),
        SPH_MATCH_ANY => t('Match any of the query words'),
        SPH_MATCH_PHRASE => t('Match the exact phrase'),
        SPH_MATCH_EXTENDED2 => t('Matches a Sphinx extended query'),
      ),
      '#default_value' => $handler->options['matchMode'],
    );
  }

  /**
   * Override of initClient().
   */
  function initClient() {
    $this->sphinxIncludeClient();
    $client = new SphinxClient();
    $host = variable_get('sphinx_default_server', 'localhost');
    $port = variable_get('sphinx_default_port', '9312');
    $client->SetServer($host, (int)$port);
    $client->SetLimits(0, 500);
    return $client;
  }

  /**
   * Escape the query string
   */
  function EscapeSphinxQL($string) {
    $from = array('\\', '(', ')', '|', '-', '!', '@', '~', '"', '&', '/', '^', '$', '=', "'", "\x00", "\n", "\r", "\x1a");
    $to   = array('\\\\', '\\\(', '\\\)', '\\\|', '\\\-', '\\\!', '\\\@', '\\\~', '\\\"', '\\\&', '\\\/', '\\\^', '\\\$', '\\\=', "\\'", "\\x00", "\\n", "\\r", "\\x1a");
    return str_replace($from, $to, $string);
  }

  /**
   * Override of executeQuery().
   */
  function executeQuery(&$client, $datasource, $query = '', $escape = TRUE) {
//    if ($escape) {
//      // Escape the query string
//      $query = $this->EscapeSphinxQL($query);
//    }
    $result = $client->Query($query, $this->sphinxGetIndex($datasource));
    if (!empty($result['matches'])) {
      return array('result' => array_keys($result['matches']), 'total' => $result['total'], 'raw' => $result);
    }
    return FALSE;
  }

  /**
   * Override of facetBuild().
   */
  function facetBuild(&$client, $datasource, $query = '', $facets) {
    $client = clone $client;
    foreach ($facets as $facet) {
      // Get fields info
      if (!empty($datasource->fields) && $datasource->fields[$facet['field']]) {
        $field = $datasource->fields[$facet['field']];
        $limit = $facet['limit'];
        // TODO: FIX!!
        switch ($field['type']) {
          case 'timestamp':
            $groupby = array('day' => SPH_GROUPBY_DAY, 'month' => SPH_GROUPBY_MONTH, 'year' => SPH_GROUPBY_YEAR);
            $granularity = !empty($facet['granularity']) ? $facet['granularity'] : 'month';
            $client->SetGroupBy($field['name'], $groupby[$granularity], '@group desc');
            $client->SetArrayResult(FALSE);
            break;
          case 'mva':
            $client->SetGroupBy($facet['field'], SPH_GROUPBY_ATTR, '@count desc');
            $client->SetArrayResult(TRUE);
            break;
          default:
            $client->SetGroupBy($facet['field'], SPH_GROUPBY_ATTR, '@count desc');
            $client->SetArrayResult(FALSE);
            break;
        }
        // Set result limit.
        if (!empty($limit)) {
          $client->SetLimits(0, (int) $limit);
        }
        $client->AddQuery($query, $this->sphinxGetIndex($datasource));
      }
    }
    // Execute queries.
    $results = $client->RunQueries();
    // Build facet results.
    $built = array();
    foreach (array_values($facets) as $num => $facet) {
      if (!empty($results[$num]['matches'])) {
        $field = $datasource->fields[$facet['field']];
        foreach (array_keys($results[$num]['matches']) as $id) {
          $attr = $results[$num]['matches'][$id]['attrs'];
          if (isset($attr['@groupby'], $attr['@count'])) {
            switch ($field['type']) {
              case 'text':
                $id = $this->sphinxGetOrdinal($datasource, $field['name'], $attr['@groupby']);
                break;
              case 'timestamp':
                $id = $this->sphinxGetTimestamp($attr['@groupby'], !empty($facet['granularity']) ? $facet['granularity'] : 'month');
                break;
              default:
                $id = $attr['@groupby'];
                break;
            }
            $built[$facet['field']][$id] = array('id' => $id, 'count' => $attr['@count']);
          }
        }
      }
    }
    return $built;
  }

  /**
   * Override of setFilter().
   */
  function setFilter(&$client, $datasource, $filters) {
    // Hardwired delete filter. See sphinxDatasourceConf() for how this is
    // added to the sphinx index.
    $filters['sphinx_deleted'] = array(
      'field' => 'sphinx_deleted',
      'operator' => '=',
      'args' => array(0),
    );
    if (!empty($filters)) {
      // Iterate through once to separate out range filters.
      $range_filters = array();
      foreach ($filters as $key => $params) {
        $field = $params['field'];
        $operator = $params['operator'];
        $args = $params['args'];
        if (in_array($operator, array('<', '>', '>=', '<=', 'BETWEEN'))) {
          unset($filters[$key]);
          switch ($operator) {
            case '>':
            case '>=':
              $range_filters[$field]['field'] = $field;
              $range_filters[$field]['max'] = isset($range_filters[$field]['max']) ? $range_filters[$field]['max'] : SPHINX_FILTER_MAX;
              $range_filters[$field]['min'] = eval("return {$args['value']};");
              $range_filters[$field]['exclude'] = $operator === '>' ? TRUE : FALSE;
              break;
            case '<':
            case '<=':
              $range_filters[$field]['field'] = $field;
              $range_filters[$field]['max'] = eval("return {$args['value']};");
              $range_filters[$field]['min'] = isset($range_filters[$field]['min']) ? $range_filters[$field]['min'] : SPHINX_FILTER_MIN;
              $range_filters[$field]['exclude'] = $operator === '<' ? TRUE : FALSE;
              break;
            case 'BETWEEN':
              $range_filters[$field]['field'] = $field;
              $range_filters[$field]['max'] = eval("return {$args['max']};");
              $range_filters[$field]['min'] = eval("return {$args['min']};");
              $range_filters[$field]['exclude'] = FALSE;
              break;
            default:
              break;
          }
        }
      }

      // Now set range filters.
      foreach ($range_filters as $filter) {
        // The exclude option on range filters appears to be broken in
        // Sphinx 0.9.9. Update this code to support either operator once the
        // problem is fixed upstream.
        // $client->SetFilterRange($filter['field'], $filter['min'], $filter['max'], $filter['exclude']);
        $client->SetFilterRange($filter['field'], $filter['min'], $filter['max'], FALSE);
      }

      // Equality & set filters filters.
      foreach ($filters as $params) {
        $field    = $params['field'];
        $operator = $params['operator'];
        $args     = $params['args'];
        switch ($operator) {
          case '=':
            $this->sphinxSetFilter($client, $datasource, $field, $args, FALSE);
            break;
          case 'IN':
            $this->sphinxSetFilter($client, $datasource, $field, $args, FALSE);
            break;
          case '<>':
          case 'NOT IN':
            $this->sphinxSetFilter($client, $datasource, $field, $args, TRUE);
            break;
          case 'IS NOT NULL':
            $this->sphinxSetFilter($client, $datasource, $field, array(0), TRUE);
            break;
        }
      }
    }
  }

  /**
   * Override of setOptions().
   */
  function setOptions(&$client, $datasource, $options) {
    if (isset($options['matchMode'])) {
      $client->SetMatchMode($options['matchMode']);
    }
  }

  /**
   * Override of setSort().
   */
  function setSort(&$client, $datasource, $sorts) {
    if (!empty($sorts)) {
      $sphinx_sorts = array();
      foreach ($sorts as $sort) {
        if ($sort['field'] === 'sphinx_weight') {
          $sphinx_sorts[] = "@weight {$sort['direction']}";
        }
        else {
          $sphinx_sorts[] = "{$sort['field']} {$sort['direction']}";
        }
      }
      $client->setSortMode(SPH_SORT_EXTENDED, implode(', ', $sphinx_sorts));
    }
  }

  /**
   * Override of setPager().
   */
  function setPager(&$client, $offset, $limit) {
    $limit = !empty($limit) ? $limit : 500;
    if (!empty($offset) || !empty($limit)) {
      $client->SetLimits((int) $offset, (int) $limit);
    }
  }

  /**
   * Return a directory where the configuration files and indexes are stored.
   *
   * This can be overridden by specifying the '--sl-sphinx-conf-path' command line option to drush.
   */
  function drushGetConfigPath() {
    return drush_get_option('sl-sphinx-conf-path', dirname(drush_locate_root()) . '/sphinx');
  }

  /**
   * Return the file name of the generated configuration file.
   */
  function drushGetConfigFile() {
    return $this->drushGetConfigPath() . '/sphinx.merged.conf';
  }

  /**
   * Generate a merged configuration file.
   */
  function drushGenerateMergedConfig() {
    $conf_path = $this->drushGetConfigPath();
    $conf_file = $this->drushGetConfigFile();

    if (file_check_directory($conf_path, TRUE)) {
      $files = array();

      // rerieve list of possible configuration file matches.
      $files[] = $conf_path . '/sphinx.conf';

      $matches = drush_scan_directory($conf_path . '/index.d/', '/^.*\.conf$/', array('.', '..'), 0, FALSE, 'name');
      sort($matches);
      foreach ($matches as $name => $file) {
        // Check if it is active datasource
        if (sphinx_advanced_existing_datasource($file->name)) {
          $files[] = $file->filename;
        }
      }

      $string = '';
      if (sizeof($files)) {
        foreach ($files as $filename) {
          if (file_exists($filename) && is_readable($filename)) {
            // concatenate contents of valid configuration file.
            $string .= "\n" . file_get_contents($filename);
          }
        }
      }

      // load the previous file
      $old_config = (file_exists($conf_file)) ? file_get_contents($conf_file) : '';

      if ($old_config != $string) {
        file_put_contents($conf_file, $string);
        return drush_log(dt("Regenerated the Sphinx configuration file"), 'success');
      }
      else {
        drush_log(dt("Sphinx configuration file is up to date"), 'success');
      }
    }
    return FALSE;
  }

  /**
   * Override of drushSearchd().
   */
  function drushSearchd($command = 'start') {
    $file_path = $this->drushGetConfigPath() . '/log';

    $lock_file = "{$file_path}/searchd.lock";
    if (file_check_directory($file_path, TRUE)) {
      $lock = file_exists($lock_file) ? file_get_contents($lock_file) : 0;

      if ((int) $lock == 1) {
        return drush_log('Search demon is already being restarted', 'error');
      }

      file_put_contents($lock_file, '1');
      if (file_check_directory($file_path, TRUE)) {
        $pid_file = "{$file_path}/searchd.pid";
        $pid = file_exists($pid_file) ? file_get_contents($pid_file) : '';

        if ((int) $pid) {
          // stop the daemon first
          drush_op('drush_shell_exec', 'searchd --nodetach --stop -c ' . $this->drushGetConfigFile());
          drush_log('Search daemon stopped.', 'success');
        }

        if ($command != 'stop') {
          // start the daemon
          drush_op('drush_shell_exec', 'searchd -c ' . $this->drushGetConfigFile());
          drush_log('Search daemon started.', 'success');
        }
      }
      file_put_contents($lock_file, '0');
      return;
    }
  }

  /**
   * Override of drushIndex().
   */
  function drushIndex($index_name = NULL, $force = TRUE) {
    // Determine if we should update the index.
    $changed = $this->drushGenerateMergedConfig();

    // Create the sphinx directory if it doesn't exist.
    $file_path = $this->drushGetConfigPath() . '/indexes';
    if (($force || $changed) && file_check_directory($file_path, TRUE)) {
      // Build list of this site's indexes.
      $indexes = array();
      $datasources = sphinx_advanced_existing_datasource($index_name);
      if ($datasources) {
        if (is_array($datasources)) {
          foreach ($datasources as $datasource) {
            $indexes[$datasource] = $datasource;
          }
        }
        else $indexes[$index_name] = $index_name;
      }
      else {
        return drush_log('Found no index to rebuild', 'error');
      }
      drush_op('drush_shell_exec', 'indexer --rotate --config ' . $this->drushGetConfigFile() . ' ' . implode(' ', $indexes));
      $output = drush_shell_exec_output();
      foreach ($output as $line) {
        drush_print($line);
      }

      // Clear caches so that sphinx id -> facet name mapping is updated.
      drush_log('Indexing complete.', 'success');
      if ($changed === TRUE) {
        return $this->drushSearchd();
      }
    }
    else {
      if (!$changed) {
        return drush_log('Indexing skipped.', 'ok');
      }
      else {
        return drush_log('An error ocurred while indexing.', 'error');
      }
    }
  }

  /**
   * Override of drushConf().
   */
  function drushConf() {
    $file_path = $this->drushGetConfigPath();
    $conf_path = $file_path .'/index.d';

    if (file_check_directory($file_path, TRUE) && drush_get_option('sl-sphinx-base-conf', TRUE)) {
      $this->drushBaseConf();
    }

    if (file_check_directory($conf_path, TRUE)) {
      // Collect configuration arrays for each datasource.
      foreach (sphinx_advanced_datasource_load(NULL) as $datasource) {
        $source = $this->sphinxDatasourceConf($datasource);
        $conf_file = "{$conf_path}/{$source['conf']['id']}.conf";
        // Generate configuration file from datasource.
        $sphinx_conf = theme('sphinx_advanced_sphinx_index_conf', array('datasource' => $source));
        $existing = file_exists($conf_file) ? file_get_contents($conf_file) : '';
        if ($sphinx_conf !== $existing) {
          file_put_contents($conf_file, $sphinx_conf);
          drush_log("{$conf_file} was written successfully.", 'success');
        }
      }
      return drush_log("Sphinx configuration files were written", 'success');
    }
    return drush_log("Sphinx configuration files could not be written.", 'error');
  }

  /**
   * Generate the base configuration for all sphinx instances.
   */
  function drushBaseConf() {
    $this->settingsDefault();
    $file_path = $this->drushGetConfigPath();
    $conf_file = "{$file_path}/sphinx.conf";
    if (file_check_directory($file_path, TRUE)) {
      $sphinx_conf = theme('sphinx_advanced_sphinx_conf', array('searchd' => $this->sphinxSearchdConf()));
      if ($sphinx_conf) {
        $existing = file_exists($conf_file) ? file_get_contents($conf_file) : '';
        if ($sphinx_conf === $existing) {
          return drush_log("{$conf_file} is unchanged.", 'success');
        }
        else {
          file_put_contents($conf_file, $sphinx_conf);
          return drush_log("{$conf_file} was written successfully.", 'success');
        }
      }
    }
  }

  /**
   * Execute functionality on drush cron run.
   */
  function drushCron() {
    $this->drushConf();
    $this->drushIndex(FALSE);
  }


  /**
   * Methods specific to the Sphinx backend ===================================
   */

  /**
   * Wrapper of $client->SetFilter().
   * Convert any string values to ordinals before passing through.
   */
  function sphinxSetFilter(&$client, $datasource, $attribute, $values, $exclude = FALSE) {
    $ordinals = array();
    foreach ((array) $values as $arg) {
      $arg = trim($arg);
      // If it is numeric or md5 argument, use it or ordinal

      if (is_numeric($arg)) {
        $ordinals[] = $arg;
      }
      else {
        $ordinal = $this->sphinxGetOrdinal($datasource, $attribute, $arg);
        if (is_numeric($ordinal)) {
          $ordinals[] = $ordinal;
        }
      }
    }
    return !empty($ordinals) ? $client->SetFilter($attribute, $ordinals, $exclude) : FALSE;
  }

  /**
   * Generate an array representing the conf and index settings for a datasource.
   */
  function sphinxDatasourceConf($datasource) {
    $conf = array(
      'conf' => array(),
      'index' => array(),
    );

    // Configuration options.
    $conf['conf']['id'] = $datasource->machine_name;

    // Use mysql stored database credentials.
    if ($creds = drush_get_context('DRUSH_DB_CREDENTIALS')) {
      $conf['conf']['type'] = $creds['driver'] === 'mysqli' ? 'mysql' : $creds['driver'];
      $conf['conf']['sql_user'] = $creds['user'];
      $conf['conf']['sql_pass'] = $creds['pass'];
      $conf['conf']['sql_host'] = $creds['host'];
      $conf['conf']['sql_db']   = $creds['name'];
      $conf['conf']['sql_port'] = isset($creds['port']) ? $creds['port'] : '3306';

    }
    // Check for optional sql_sock option.
    $sock = trim($this->settings['sql_sock']);
    if (!empty($sock)) {
      $conf['conf']['sql_sock'] = $sock;
    }

    // Index information
    $conf['index']['path'] = $this->drushGetConfigPath() . '/indexes/' . $datasource->machine_name;
    $conf['index']['docinfo'] = 'extern';
    $conf['index']['mlock'] = 0;

    // Morphology settings.
    if (!empty($this->settings['index']['morphology'])) {
      $conf['index']['morphology'] = $this->settings['index']['morphology'];
    }
    // Charactsets, CJK handling, etc.
    if (isset($this->settings['index']['charset_table'])) {
      $conf['index']['charset_table'] = $this->settings['index']['charset_table'];
    }
    if (isset($this->settings['index']['ngram_len'])) {
      $conf['index']['ngram_len'] = $this->settings['index']['ngram_len'];
    }
    if (isset($this->settings['index']['ngram_chars'])) {
      $conf['index']['ngram_chars'] = $this->settings['index']['ngram_chars'];
    }
    // Assume UTF-8, character stripping for now.
    if (isset($this->settings['index']['charset_type'])) {
      $conf['index']['charset_type'] = $this->settings['index']['charset_type'];
    }
    if (isset($this->settings['index']['html_strip'])) {
      $conf['index']['html_strip'] = $this->settings['index']['html_strip'];
    }
    // Options to use wildcard
    if (isset($this->settings['index']['enable_star'])) {
      $conf['index']['enable_star'] = $this->settings['index']['enable_star'];
    }
    if (isset($this->settings['index']['min_prefix_len'])) {
      $conf['index']['min_prefix_len'] = $this->settings['index']['min_prefix_len'];
    }
    if (isset($this->settings['index']['min_infix_len'])) {
      $conf['index']['min_infix_len'] = $this->settings['index']['min_infix_len'];
    }
    // Force utf8 when indexing.
    $conf['conf']['sql_query_pre'] = "SET NAMES utf8";
    $conf['conf']['sql_query'] = $datasource->sql_query;
    $conf['conf']['sql_query_info'] = $datasource->sql_query_info;
    $conf['conf']['sql_query_range'] = $datasource->sql_query_range;
    $conf['conf']['sql_query_killlist'] = $datasource->sql_query_killlist;
    $conf['conf']['parent_index'] = (!empty($datasource->parent_index)) ? $datasource->parent_index : '';
    $conf['conf']['sql_range_step'] = 5000;
    $conf['conf']['sql_ranged_throttle'] = 0;
    // Merge in attributes.
    $sql_attr = array();
    $sphinx_type = array(
      'text' => 'sql_attr_str2ordinal',
      'int' => 'sql_attr_uint',
      'md5' => 'sql_attr_bigint',
      'float' => 'sql_attr_float',
      'timestamp' => 'sql_attr_timestamp',
      'string' => 'sql_attr_string',
    );

    // Hardwired delete flag.
    $sql_attr[] = "{$sphinx_type['int']} = sphinx_deleted";

    // Generate attribute entries.
    if ($datasource->fields) {
      foreach ($datasource->fields as $field) {
        // TODO add different types for fields
        // If field named as Id, use int attribute
        if (!empty($sphinx_type[$field['type']])) {
          $sql_attr[] = "{$sphinx_type[$field['type']]} = {$field['name']}";
        }
        elseif ($field['type'] != 'mva') $sql_attr[] = "{$sphinx_type['text']} = {$field['name']}";
      }
    }
    $sql_attr = array_unique($sql_attr);
    $conf['conf']['sql_attr'] = implode(" \n", $sql_attr);

    $sql_attr_multi = $datasource->sql_attr_multi;
    if ($sql_attr_multi) $conf['conf']['sql_attr_multi'] = "\nsql_attr_multi = {$sql_attr_multi}";
    return $conf;
  }

  /**
   * Get the Sphinx searchd settings.
   */
  function sphinxSearchdConf() {
    $searchd = array();
    $searchd['port'] = $this->settings['port'];;
    $searchd['memory_limit'] = $this->settings['memory_limit'];
    $searchd['write_buffer'] = $this->settings['write_buffer'];
    $searchd['log'] = $this->drushGetConfigPath() . '/log/searchd.log';
    $searchd['query_log'] = $this->drushGetConfigPath() . '/log/query.log';
    $searchd['pid_file'] = $this->drushGetConfigPath() . '/log/searchd.pid';
    return $searchd;
  }

  /**
   * Get the ordinal integer for a given string, or vice versa.
   */
  function sphinxGetOrdinal($datasource, $attribute_name, $value, $reset = FALSE) {
    $datasource_id = $datasource->machine_name;
    if (!isset($this->ordinals[$datasource_id]) || $reset) {
      $cid = "sphinx_advanced_ordinals_{$datasource_id}";
      if (!$reset && $cache = cache_get($cid)) {
        $this->ordinals[$datasource_id] = $cache->data;
      }
      if ($reset || !isset($this->ordinals[$datasource_id])) {
        $this->ordinals[$datasource_id] = $this->sphinxGetOrdinals($datasource);
        if (!empty($this->ordinals[$datasource_id])) {
          cache_set($cid, $this->ordinals[$datasource_id]);
        }
      }
    }

    if (is_numeric($value)) {
      return isset($this->ordinals[$datasource_id][$attribute_name]) ? array_search($value, $this->ordinals[$datasource_id][$attribute_name]) : FALSE;
    }
    else {
      return isset($this->ordinals[$datasource_id][$attribute_name][$value]) ? $this->ordinals[$datasource_id][$attribute_name][$value] : FALSE;
    }
  }

  /**
   * Get an ordinal mapping for this datasource.
   * As this is a very expensive operation, its results are cached and
   * used in the ->sphinxGetOrdinal() method. You should only call this method
   * if you know what you are doing.
   */
  function sphinxGetOrdinals($datasource) {
    // Retrieve all ordinal attributes.
    $attributes = array();
    if (!empty($datasource->fields)) {
      foreach ($datasource->fields as $field) {
        $attributes[$field['name']] = $field;
      }
    }
    // If there are attributes with ordinals, generate a mapping.
    // We only handle string ordinals for sets of less than 1000 (for now).
    $ordinals = array();
    if (!empty($attributes)) {
      $this->sphinxIncludeClient();
      $sphinx = new SphinxClient();
      $sphinx->SetLimits(0, 1000);
      foreach ($attributes as $field) {
        // Dispatch a Sphinx query to retrieve the ordinal.
        $sphinx->SetGroupBy($field['name'], SPH_GROUPBY_ATTR, '@count desc');
        $sphinx->SetGroupDistinct($field['name']);
        $result = $sphinx->Query(NULL, $this->sphinxGetIndex($datasource));
        if (!empty($result['matches']) && count($result['matches']) < 1000) {
          $ids = array_keys($result['matches']);
          // Get attribute data from Query
          foreach ($ids as $id) {
            $query = $datasource->sql_query;
            $query = str_replace("\\", '' , str_replace('$end', $id, str_replace('$start', $id, $query)));
            $row = db_fetch_object(db_query($query));
            if ($row && $row->$field['name']) {
              $ordinals[$field['name']][$row->$field['name']] = $result['matches'][$id]['attrs'][$field['name']];
            }
          }

        }
      }
    }
    return $ordinals;
  }

  /**
   * Convert a Sphinx datestring into a timestamp. Requires a datestring
   * (e.g. 201010) and a target granularity: day, month, year. We reverse-apply
   * Drupal's timezone offset so that this date can be used with format_date(),
   * etc. cleanly.
   */
  function sphinxGetTimestamp($datestring, $granularity) {
    $timezone = date_default_timezone(TRUE);
    switch ($granularity) {
      case 'day':
        return ($timezone * -1) + mktime(0, 0, 0, substr($datestring, 4, 2), substr($datestring, 6, 2), substr($datestring, 0, 4));
      case 'month':
        return ($timezone * -1) + mktime(0, 0, 0, substr($datestring, 4, 2), 1, substr($datestring, 0, 4));
      case 'year':
        return ($timezone * -1) + mktime(0, 0, 0, 1, 1, $datestring);
    }
  }

  /**
   * Return the index to query.
   */
  function sphinxGetIndex($datasource) {
    // Select delta indexes if available
    if ($datasource->child_index) {
      $search_index = $datasource->machine_name . ',' . $datasource->child_index;
    }
    elseif ($datasource->parent_index) {
      $search_index = $datasource->parent_index . ',' . $datasource->machine_name;
    }
    else $search_index = $datasource->machine_name;
    return $search_index;
  }

  /**
   * Get the renderer for a given field, or vice versa.
   */
  function sphinxGetRenderer($datasource, $attribute_name, $value) {
    $datasource_id = $datasource->machine_name;

    if (!isset($this->renderer[$datasource_id])) {
      $this->renderer[$datasource_id] = array();
    }

    if (!isset($this->renderer[$datasource_id][$attribute_name])) {
      $this->renderer[$datasource_id][$attribute_name] = array();
    }

    // Get rendered value from render callback
    if (!isset($this->renderer[$datasource_id][$attribute_name][$value])) {
      // Get render callback
      $render = $datasource->fields[$attribute_name]['renderer'];
      if (!empty($render) && is_callable($render)) {
        // Get rendered value
        $this->renderer[$datasource_id][$attribute_name][$value] = call_user_func($render, $value);
      }
    }
    return isset($this->renderer[$datasource_id][$attribute_name][$value]) ? $this->renderer[$datasource_id][$attribute_name][$value] : $value;
  }
}
