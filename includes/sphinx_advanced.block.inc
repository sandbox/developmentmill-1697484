<?php
// $Id$
/**
 * @file
 * Include for search block for the sphinx_advanced module.
 */

/**
 * Form builder; Output a search form for the search block and the theme's search box.
 *
 * @ingroup forms
 * @see search_box_form_submit()
 * @see theme_search_box_form()
 */
function sphinx_advanced_search_form(&$form_state) {
  // Get search string
  $form_state['post']['search_block_form'] = isset($form_state['post']['search_block_form']) ? $form_state['post']['search_block_form'] : arg(2);

  $form['search_block_form'] = array(
    '#title' => t('Search'),
    '#type' => 'textfield',
    //'#size' => 15,
    '#default_value' => isset($form_state['post']['search_block_form']) ? $form_state['post']['search_block_form'] : '',
    '#attributes' => array('title' => t('Enter the terms you wish to search for.')),
  );

  $query = array();
  if (!isset($form_state['post']['search_block_form']) || $form_state['post']['search_block_form'] == '') $current_page = $_GET['q'];
  else $current_page = str_replace('/' . $form_state['post']['search_block_form'], '', $_GET['q']);
  $form['redirect_url'] = array(
    '#value' => $current_page,
    '#type' => 'hidden',
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Search'));
  $form['#submit'][] = 'sphinx_advanced_search_form_submit';

  return $form;
}

/**
 * Process a block search form submission.
 */
function sphinx_advanced_search_form_submit($form, &$form_state) {
  $form_id = $form['form_id']['#value'];
  $redirect = $_REQUEST['redirect_url'];
  // Redirect with new keywords
  $form_state['redirect'] = $redirect . '/'. trim($form_state['values']['search_block_form']);
}

/**
 * Implements of hook_block().
 */
function sphinx_advanced_block($op = 'list', $delta = 0) {
  if ($op == 'list') {
    $blocks[0]['info'] = t('Sphinx Advanced Search form');
    // Not worth caching.
    $blocks[0]['cache'] = BLOCK_NO_CACHE;
    return $blocks;
  }
  elseif ($op == 'view' && user_access('search content')) {
    if ($delta == 0) $block['content'] = drupal_get_form('sphinx_advanced_search_form');
    //$block['subject'] = t('Sphinx Advanced Search');
    return $block;
  }
}

/**
 * Implements hook_form_views_exposed_form_alter().
 *
 */
function sphinx_advanced_form_views_exposed_form_alter(&$form, $form_state) {
  global $_sphinx_advanced_facets;
  $display_options = $form_state['view']->display_handler->options;
  if ($display_options['use_sphinx'] && !empty($display_options['sphinx_index']) && $_sphinx_advanced_facets) {
    foreach ($_sphinx_advanced_facets as $facet_name => $facet) {
      $facet_list = array();
      $selected = array();
      foreach ($facet['values'] as $value) {
        if ($value['active'] == TRUE && $facet['type'] == 'mva') {
          $selected[$value['id']] = $value['id'];
        }
        elseif ($value['active'] == TRUE && $facet['type'] != 'mva') {
          $selected = $value['id'];
        }

        if ($facet['type'] != 'mva') {
          // Add default value for single select list
          $facet_list[0] = 'None';
          if (empty($selected)) $selected = 0;
        }
        // Print facet title
        $facet_list[$value['id']] = $value['title'] . ' (' . $value['count'] . ')';
      }
      // Build facet form
      if ($facet['context_field']) {
        $form['facets'][$facet_name] = array(
          '#type' => 'fieldset',
          '#collapsible' => TRUE,
          '#title' => (!empty($facet['facet_title'])) ? $facet['facet_title'] : t('Facet ' . $facet_name),
        );

        $form['facets'][$facet_name][$facet['context_field']] = array(
          '#type' => 'textfield',
          //'#title' => (!empty($facet['facet_title'])) ? $facet['facet_title'] : t('Facet ' . $facet_name),
          '#default_value' => '',
        );
        $form['facets'][$facet_name][$facet_name] = array(
          '#type' => ($facet['type'] == 'mva') ? 'checkboxes' : 'select',
          '#multiple' => FALSE,
          '#options' => $facet_list,
          '#default_value' => (!empty($selected)) ? $selected : '',
        );
      }
      else {
        $form['facets'][$facet_name] = array(
          '#type' => ($facet['type'] == 'mva') ? 'checkboxes' : 'select',
          '#multiple' => FALSE,
          '#title' => (!empty($facet['facet_title'])) ? $facet['facet_title'] : t('Facet ' . $facet_name),
          '#options' => $facet_list,
          '#default_value' => (!empty($selected)) ? $selected : '',
        );
      }

      // Selected facets
      if (!empty($selected))  $form_state['input']['facets'][$facet_name] = $selected;
    }
  }
}

/**
 * Implements hook_form_alter().
 *
 */
function sphinx_advanced_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_theme_form') {
    // Override site search URL
    $form['#submit'] = array('sphinx_advanced_searchbox_form_submit');
  }
}

/**
 * Process a block search form submission.
 */
function sphinx_advanced_searchbox_form_submit($form, &$form_state) {
  // The search form relies on control of the redirect destination for its
  // functionality, so we override any static destination set in the request,
  // for example by drupal_access_denied() or drupal_not_found()
  // (see http://drupal.org/node/292565).
  if (isset($_REQUEST['destination'])) {
    unset($_REQUEST['destination']);
  }
  if (isset($_REQUEST['edit']['destination'])) {
    unset($_REQUEST['edit']['destination']);
  }

  $form_id = $form['form_id']['#value'];
  $form_state['redirect'] = 'site/search/'. trim($form_state['values'][$form_id]);
}
