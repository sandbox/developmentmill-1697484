<?php
// $Id$
/**
 * @file
 * Include for renderers for the sphinx_advanced module.
 */

/**
 * Implements hook_sphinx_renderers_info().
 *
 */
function sphinx_advanced_sphinx_renderers_info() {
  return array(
    'theme_node_type' => array(
      'name' => t('Node type'),
      'description' => t('Renderer for custom fields in my index'),
    ),
    'theme_node_uid' => array(
      'name' => t('User name'),
      'description' => t('Renderer for custom fields in my index'),
    ),
    'theme_node_author' => array(
      'name' => t('Author name'),
      'description' => t('Renderer for custom fields in my index'),
    ),
    'theme_journal_term' => array(
      'name' => t('Journal term'),
      'description' => t('Renderer for custom fields in my index'),
    ),
    'theme_node_created' => array(
      'name' => t('Created time'),
      'description' => t('Renderer for custom fields in my index'),
    ),
    'theme_node_title' => array(
      'name' => t('Node title'),
      'description' => t('Renderer for custom fields in my index'),
    ),
    'theme_node_classification' => array(
      'name' => t('Field Classification'),
      'description' => t('Renderer for custom fields in my index'),
    ),
  );
}

/* Renderers for Sphinx index attribures */
function theme_node_type($value) {
  $types = node_get_types('types', NULL, TRUE);
  foreach ($types as $type) {
    if (md5($type->type) == $value) {
      return $type->type;
    }
  }
  return $value;
}

function theme_node_uid($value) {
  if (is_numeric($value)) {
    $user = user_load($value);
    if ($user) return $user->name;
  }
  return $value;
}

function theme_node_author($value) {
  if (is_numeric($value)) {
    $author = db_result(db_query('SELECT name FROM {biblio_contributor_data} WHERE cid = %d', $value));
    if ($author) return $author;
  }
  return $value;
}

function theme_journal_term($value) {
  if (is_numeric($value)) {
    $author = db_result(db_query('SELECT name FROM {term_data} WHERE tid = %d', $value));
    if ($author) return $author;
  }
  return $value;
}

function theme_node_title($value) {
  if (is_numeric($value)) {
    $author = db_result(db_query('SELECT title FROM {node} WHERE nid = %d', $value));
    if ($author) return $author;
  }
  return $value;
}

function theme_node_created($value) {
  if (is_numeric($value)) {
    return date('d-m-Y', $value);
  }
  return $value;
}

function theme_node_classification($value) {
  if ($value == md5('Field')) {
    return 'Field';
  }
  elseif ($value == md5('Topic')) {
    return 'Topic';
  }
  elseif ($value == md5('Series')) {
    return 'Series';
  }
  return $value;
}
