<?php
// $Id$
/**
 * @file
 * Drush implementation for the sphinx_advanced module.
 */

/**
 * Implements hook_drush_command().
 */
function sphinx_advanced_drush_command() {
  $items = array();
  $items['sphinx-advanced-rebuild'] = array(
    'callback' => 'drush_sphinx_advanced_rebuild',
    'description' => 'Rebuild the Sphinx configuration for the current site.',
    'drupal dependencies' => array('sphinx_advanced'),
  );
  $items['sphinx-advanced-reindex'] = array(
    'callback' => 'drush_sphinx_advanced_reindex',
    'description' => 'Rebuild indexes for the current site.',
    'arguments' => array(
      'index' => 'The name of index to reindex.',
    ),
    'drupal dependencies' => array('sphinx_advanced'),
  );

  $items['sphinx-advanced-searchd'] = array(
    'callback' => 'drush_sphinx_advanced_searchd',
    'description' => 'Start the backend search daemon for the current site.',
    'drupal dependencies' => array('sphinx_advanced'),
  );
  return $items;
}

/**
 * Rebuild the Sphinx configuration and indexes.
 */
function drush_sphinx_advanced_rebuild() {
  $backend = sphinx_advanced_get_backend();
  // Rebuild config
  $backend->drushConf();
  $backend->drushGenerateMergedConfig();
}

/**
 * Rebuild the indexes.
 */
function drush_sphinx_advanced_reindex($index = NULL) {
  $backend = sphinx_advanced_get_backend();
  // Rebuild given index
  $backend->drushIndex($index);
}

/**
 * Start or stop the search daemon.
 */
function drush_sphinx_advanced_searchd($command = 'start') {
  $backend = sphinx_advanced_get_backend();
  $backend->drushSearchd($command);
}