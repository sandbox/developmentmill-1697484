<?php
// $Id$
/**
 * @file
 * Administration interface for the sphinx_advanced module.
 */

/**
 * Menu callback; Administer Sphinx indexes
 */
function sphinx_advanced_settings_page() {
  return drupal_get_form('sphinx_advanced_settings_form');
}

/**
 * FAPI definition for the sphinx administration form.
 *
 */
function sphinx_advanced_settings_form() {
  $form = array();
  $form['sphinx_default_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Default server'),
    '#default_value' => variable_get('sphinx_default_server', 'localhost'),
    '#description' => t('Type the ip-address of the server where you main searchd is running (this setting can be altered per index)'),
  );
  $form['sphinx_default_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Default port'),
    '#default_value' => variable_get('sphinx_default_port', '3312'),
    '#description' => t('Type the port on which your default searchd is listening (this setting can be altered per index)'),
  );
  return system_settings_form($form);
}

/**
 * Menu callback; Administer Sphinx indexes
 */
function sphinx_advanced_indexes_page($op = '', $index = '') {
  // Set info message
  $info_message = 'Don\'t forget to rebuild Sphinx config and index after making changes.';
  $messages = drupal_get_messages('status', FALSE);
  if (empty($messages) || !in_array($info_message, $messages['status'])) {
    drupal_set_message($info_message);
  }

  if (!empty($op) && !empty($index)) {
    switch ($op) {
      case 'delete':
        return drupal_get_form('sphinx_advanced_index_confirm_delete', $index);
      break;

      case 'edit':
        return drupal_get_form('sphinx_advanced_index_edit', $index);
      break;

      case 'enable':
        db_query('UPDATE {sphinx_advanced_indexes} SET status = 1 WHERE machine_name = "%s"', $index);
        drupal_goto('admin/settings/sphinx_advanced/advanced_indexes');
        break;

      case 'disable':
        db_query('UPDATE {sphinx_advanced_indexes} SET status = 0 WHERE machine_name = "%s"', $index);
        drupal_goto('admin/settings/sphinx_advanced/advanced_indexes');
        break;

      break;
    }
  }

  $header = array(
    array('data' => t('Machine name'), 'field' => 'machine_name'),
    array('data' => t('Index name'), 'field' => 'name'),
    array('data' => t('Active'), 'field' => 'status'),
  );

  $sql     = 'SELECT machine_name, name, status, parent_index FROM {sphinx_advanced_indexes}' . tablesort_sql($header);
  $result  = db_query($sql);
  $backend = sphinx_advanced_get_backend();
  $client = $backend->initClient();
  if (method_exists($client, 'Open')) {
    $connect = $client->Open();
  }
  else {
    $connect = $client->_Connect();
  }
  $index_list = array(0 => 'None');
  while ($indexes = db_fetch_object($result)) {
    if ($indexes->status) {
      $client->SetLimits(0, 1);
      $res = NULL;
      $res = $client->Query('', $indexes->machine_name);
      if ($res) {
        $status = '<span class="ok">Serving (' . $res['total_found'] . ')</span>';
      }
      else {
        $status = '<span class="error">Error</span>';
      }
    }
    else {
      $status = '<span class="warning">Disabled</span>';
    }
    $active = ($indexes->status == 0) ? l(t('Enable'), 'admin/settings/sphinx_advanced/advanced_indexes/enable/' . $indexes->machine_name) : l(t('Disable'), 'admin/settings/sphinx_advanced/advanced_indexes/disable/' . $indexes->machine_name);
    $rows[] = array(
      $indexes->machine_name,
      $indexes->name,
      $status,
      l(t('Delete'), 'admin/settings/sphinx_advanced/advanced_indexes/delete/' . $indexes->machine_name),
      $active,
      l(t('Edit'), 'admin/settings/sphinx_advanced/advanced_indexes/edit/' . $indexes->machine_name),
    );

    if (empty($indexes->parent_index)) $index_list[$indexes->machine_name] = $indexes->name;
  }

  $output = theme('table', $header, $rows);
  $output .= drupal_get_form('sphinx_advanced_index_add', $index_list);
  //$output .= theme('pager', null, 10, 10);
  return $output;
}

/**
 * Form constructor for the index add form.
 *
 * @param $index_list
 *   List of existing indexes.
 *
 * @see sphinx_advanced_index_add_validate()
 * @see sphinx_advanced_index_add_submit()
 * @ingroup forms
 */
function sphinx_advanced_index_add($form_state, $index_list) {
  $form = array();

  $form['sphinx'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add new index'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['sphinx']['machine_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine name'),
    '#description' => t('Type the machine name of the index. This name must contain only lowercase letters, numbers, and underscores.'),
    '#required' => TRUE,
  );

  $form['sphinx']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Index name'),
    '#description' => t('Type the name you want the users to see'),
    '#required' => TRUE,
  );

  if ($index_list) {
    $form['sphinx']['parent_index'] = array(
      '#type' => 'select',
      '#title' => t('Parent index'),
      '#options' => $index_list,
      '#default_value' => isset($form_state['post']['parent_index']) ? $form_state['post']['parent_index'] : 0,
      '#description' => t('Type the name you want the users to see'),
      '#required' => TRUE,
    );
  }

  $form['sphinx']['sql_query'] = array(
    '#type' => 'textarea',
    '#title' => t('SQL Query'),
    '#description' => t('Type the SQL Query for this index'),
    '#required' => TRUE,
    '#default_value' => "SELECT node.nid AS nid, node.title as title, 0 AS sphinx_deleted \
FROM  node node \
WHERE ((node.type = 'page' AND (node.nid BETWEEN \$start AND \$end ) ))",
  );

  $form['sphinx']['context_fields'] = array(
    '#type' => 'textfield',
    '#title' => t('Context fields'),
    '#size' => 128,
    '#description' => t('Comma-separated list of context fields'),
    '#default_value' => isset($form_state['post']['context_fields']) ? $form_state['post']['context_fields'] : '',
  );

  if (isset($form_state['post']['fields_count'])) {
    $fields_count = $form_state['post']['fields_count'] + 1;
  }
  else {
    $fields_count = 1;
  }

  $form['sphinx']['fields_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sphinx attributes'),
    '#prefix' => '<div id="sphinx-fields-wrapper">',
    '#suffix' => '</div>',
  );

  $type_opts = array(
    'int' => 'int',
    'timestamp' => 'timestamp',
    'md5' => 'md5',
    'float' => 'float',
    'text' => 'text',
    'mva' => 'multi-values',
    'string' => 'string',
  );

  // Get renderers list
  $renderers = sphinx_advanced_get_renderers('name');
  $renderers[0] = 'None';
  for ($delta = 1; $delta <= $fields_count; $delta++) {
    $form['sphinx']['fields_wrapper'][$delta] = array(
      '#type' => 'fieldset',
    );
    $form['sphinx']['fields_wrapper'][$delta]['field_name_' . $delta] = array(
      '#type' => 'textfield',
      '#title' => t('Attribute name'),
      '#size' => 20,
      '#description' => t('Type attribute field name'),
      '#prefix' => '<div style="float:left; margin:1em">',
      '#suffix' => '</div>',
      '#default_value' => isset($form_state['post']['field_name_' . $delta]) ? $form_state['post']['field_name_' . $delta] : '',
    );
    $form['sphinx']['fields_wrapper'][$delta]['field_title_' . $delta] = array(
      '#type' => 'textfield',
      '#title' => t('Attribute title'),
      '#size' => 20,
      '#description' => t('Type attribute field title'),
      '#prefix' => '<div style="float:left; margin:1em">',
      '#suffix' => '</div>',
      '#default_value' => isset($form_state['post']['field_title_' . $delta]) ? $form_state['post']['field_title_' . $delta] : '',
    );
    $form['sphinx']['fields_wrapper'][$delta]['field_context_' . $delta] = array(
      '#type' => 'textfield',
      '#title' => t('Context field'),
      '#size' => 20,
      '#description' => t('Context field associated with this attr'),
      '#prefix' => '<div style="float:left; margin:1em">',
      '#suffix' => '</div>',
      '#default_value' => isset($form_state['post']['field_context_' . $delta]) ? $form_state['post']['field_context_' . $delta] : '',
    );
    $form['sphinx']['fields_wrapper'][$delta]['field_type_' . $delta] = array(
      '#type' => 'select',
      '#title' => t('Field type'),
      '#options' => $type_opts,
      '#prefix' => '<div style="float:left; margin:1em">',
      '#suffix' => '</div>',
      '#default_value' => isset($form_state['post']['field_type_' . $delta]) ? $form_state['post']['field_type_' . $delta] : '',
    );
    $form['sphinx']['fields_wrapper'][$delta]['field_renderer_' . $delta] = array(
      '#type' => 'select',
      '#options' => $renderers,
      '#title' => t('Attribute renderer'),
      '#prefix' => '<div style="float:left; margin:1em">',
      '#suffix' => '</div>',
      '#default_value' => isset($form_state['post']['field_renderer_' . $delta]) ? $form_state['post']['field_renderer_' . $delta] : 0,
    );
  }
  $form['sphinx']['fields_wrapper']['fields_count'] = array(
    '#type' => 'hidden',
    '#value' => $fields_count,
  );

  $form['sphinx']['add_more_sphinx_field'] = array(
    '#type' => 'submit',
    '#value' => t('Add field'),
    '#submit' => array('poll_more_choices_submit'), // If no javascript action.
    '#ahah' => array(
      'path' => 'sphinx_advanced_indexes/js',
      'wrapper' => 'sphinx-fields-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['sphinx']['sql_attr_multi'] = array(
    '#type' => 'textarea',
    '#title' => t('Multiple attr description'),
    '#description' => t('Type the description for multiple attr'),
    '#default_value' => '',
  );

  $form['sphinx']['sql_query_info'] = array(
    '#type' => 'textarea',
    '#title' => t('SQL Query Info'),
    '#description' => t('Type the SQL Query Info for this index'),
    '#required' => TRUE,
    '#default_value' => 'SELECT node.nid AS nid, node.title as title, 0 AS sphinx_deleted \
FROM  node node \
WHERE (( (node.nid = $id) ))',
  );

  $form['sphinx']['sql_query_range'] = array(
    '#type' => 'textarea',
    '#title' => t('SQL Query Range'),
    '#description' => t('Type the SQL Query Range for this index'),
    '#required' => TRUE,
    '#default_value' => 'SELECT MIN(nid), MAX(nid) FROM node WHERE nid > 0',
  );

  $form['sphinx']['sql_query_killlist'] = array(
    '#type' => 'textarea',
    '#title' => t('SQL Query Killlist'),
    '#description' => t('Type the SQL Query Killlist for main indexes only'),
    '#default_value' => "SELECT node.nid FROM node node \
INNER JOIN sphinx_advanced_indexes AS index_info ON index_info.machine_name = 'page_title' \
WHERE ((node.changed > index_info.last_reindex AND node.type = 'page'))",
  );

  $form['sphinx']['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable'),
    '#description' => t('Should this index be activated?'),
  );
  $form['sphinx']['submit'] = array('#type' => 'submit', '#value' => t('Submit'));

  return $form;
}

/**
 * Form validation handler for sphinx_advanced_index_add().
 *
 * @see sphinx_advanced_index_add_submit()
 */
function sphinx_advanced_index_add_validate($form, &$form_values) {
  if (!preg_match('!^[a-z0-9_]+$!', $form_values['values']['machine_name'])) {
    form_set_error('machine_name', t('The machine-readable name must contain only lowercase letters, numbers, and underscores.'));
  }
  elseif (sphinx_advanced_existing_datasource($form_values['values']['machine_name'], TRUE)) {
    form_set_error('machine_name', t('The machine-readable name %machine_name is already taken.', array('%machine_name' => $form_values['values']['machine_name'])));
  }
}

/**
 * Form submission handler for sphinx_advanced_index_add().
 *
 * @see sphinx_advanced_index_add_validate()
 */
function sphinx_advanced_index_add_submit($form_id, $form_values) {
  // Create new datasource object
  $datasource = sphinx_advanced_datasource_create($form_values['values']['machine_name']);
  if ($form_values['values']) {
    // Transform fields list
    $fields = array();
    for ($i = 1; $i <= $form_values['values']['fields_count']; $i++) {
      $field_name = trim($form_values['values']['field_name_' . $i]);
      $field_type = trim($form_values['values']['field_type_' . $i]);
      $field_title = trim($form_values['values']['field_title_' . $i]);
      $field_renderer = trim($form_values['values']['field_renderer_' . $i]);
      $field_context = trim($form_values['values']['field_context_' . $i]);
      if ($field_name) $fields[$field_name] = array('name' => $field_name, 'type' => $field_type, 'title' => $field_title, 'renderer' => $field_renderer, 'context' => $field_context);
    }
    $form_values['values']['fields'] = serialize($fields);

    // Save new datasource data
    foreach ($form_values['values'] as $key => $value) {
      if (!is_object($value)) $datasource->$key = $value;
    }
    $datasource->saveData();
  }
  drupal_goto('admin/settings/sphinx_advanced/advanced_indexes');
}

/**
 * Form submission handler for index deletion().
 *
 * @see sphinx_advanced_index_confirm_delete_submit()
 */
function sphinx_advanced_index_confirm_delete($values, $index = '') {
  if (!empty($index)) {
    $datasource = sphinx_advanced_datasource_load($index, TRUE);
    if ($datasource) {
      $form = array();
      $form['datasource']  = array('#type' => 'hidden', '#value' => $datasource);
      $form['text'] = array(
        '#value' => t('Are you sure you want to delete the index <strong>%title</strong>? ', array('%title' => $datasource->name)),
      );
      $form['submit'] = array('#type' => 'submit', '#value' => t('Delete'));
      $form['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'));
      return $form;
    }
  }
}

/**
 * Form submission handler for sphinx_advanced_index_confirm_delete().
 */
function sphinx_advanced_index_confirm_delete_submit($form_id, $form_values) {
  if ($form_values['values']['op'] == 'Delete') {
    // Delete datasource
    $datasource = $form_values['values']['datasource'];
    $datasource->deleteData();
  }
  drupal_goto('admin/settings/sphinx_advanced/advanced_indexes');
}

/**
 * Form constructor for the index edit form.
 *
 * @param $index
 *   Index name.
 *
 * @see sphinx_advanced_index_edit_validate()
 * @see sphinx_advanced_index_edit_submit()
 * @ingroup forms
 */
function sphinx_advanced_index_edit($form_state, $index = '') {
  if (!empty($index)) {
    // Get current index data
    $datasource = sphinx_advanced_datasource_load($index, TRUE);
    if ($datasource) {
      $datasource->loadData();

      // Get other indexes names
      $sql = db_query('SELECT machine_name, name, parent_index FROM {sphinx_advanced_indexes}');
      $index_list = array(0 => 'None');
      while ($in = db_fetch_array($sql)) {
        if (empty($in['parent_index']) && $in['machine_name'] != $index) $index_list[$in['machine_name']] = $in['name'];
      }

      // Count index attribute fields
      if ($datasource->fields) {
        if (!isset($form_state['post']['fields_count'])) $form_state['post']['fields_count'] = count($datasource->fields);
      }
      $form = array();
      $form['sphinx']['machine_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Machine name'),
        '#description' => t('Type the machine name of the index. This name must contain only lowercase letters, numbers, and underscores.'),
        '#required' => TRUE,
        '#default_value' => isset($form_state['post']['machine_name']) ? $form_state['post']['machine_name'] : $datasource->machine_name,
      );
      $form['sphinx']['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Index name'),
        '#description' => t('Type the name you want the users to see'),
        '#required' => TRUE,
        '#default_value' => isset($form_state['post']['name']) ? $form_state['post']['name'] : $datasource->name,
      );

      if ($index_list) {
        $form['sphinx']['parent_index'] = array(
          '#type' => 'select',
          '#title' => t('Parent index'),
          '#options' => $index_list,
          '#default_value' => isset($form_state['post']['parent_index']) ? $form_state['post']['parent_index'] : $datasource->parent_index,
          '#description' => t('Type the name you want the users to see'),
          '#required' => TRUE,
        );
      }

      $form['sphinx']['sql_query'] = array(
        '#type' => 'textarea',
        '#title' => t('SQL Query'),
        '#description' => t('Type the SQL Query for this index'),
        '#required' => TRUE,
        '#default_value' => isset($form_state['post']['sql_query']) ? $form_state['post']['sql_query'] : $datasource->sql_query,
      );

      $form['sphinx']['context_fields'] = array(
        '#type' => 'textfield',
        '#title' => t('Context fields'),
        '#size' => 128,
        '#description' => t('Comma-separated list of context fields'),
        '#default_value' => isset($form_state['post']['context_fields']) ? $form_state['post']['context_fields'] : $datasource->context_fields,
      );

      if (isset($form_state['post']['fields_count'])) {
        $fields_count = $form_state['post']['fields_count'] + 1;
      }
      else {
        $fields_count = 1;
      }

      $form['sphinx']['fields_wrapper'] = array(
        '#type' => 'fieldset',
        '#title' => t('Sphinx attributes'),
        '#prefix' => '<div id="sphinx-fields-wrapper">',
        '#suffix' => '</div>',
      );

      $type_opts = array(
        'int' => 'int',
        'timestamp' => 'timestamp',
        'md5' => 'md5',
        'float' => 'float',
        'text' => 'text',
        'mva' => 'multi-values',
        'string' => 'string',
      );

      // Get renderers list
      $renderers = sphinx_advanced_get_renderers('name');
      $renderers[0] = 'None';
      for ($delta = 1; $delta <= $fields_count; $delta++) {
        $form['sphinx']['fields_wrapper'][$delta] = array(
          '#type' => 'fieldset',
        );
        // Get attribute info
        $field = array_shift($datasource->fields);
        $form['sphinx']['fields_wrapper'][$delta]['field_name_' . $delta] = array(
          '#type' => 'textfield',
          '#title' => t('Attribute name'),
          '#size' => 20,
          '#description' => t('Type attribute field name'),
          '#prefix' => '<div style="float:left; margin:1em">',
          '#suffix' => '</div>',
          '#default_value' => isset($form_state['post']['field_name_' . $delta]) ? $form_state['post']['field_name_' . $delta] : $field['name'],
        );
        $form['sphinx']['fields_wrapper'][$delta]['field_title_' . $delta] = array(
          '#type' => 'textfield',
          '#title' => t('Attribute title'),
          '#size' => 20,
          '#description' => t('Type attribute field title'),
          '#prefix' => '<div style="float:left; margin:1em">',
          '#suffix' => '</div>',
          '#default_value' => isset($form_state['post']['field_title_' . $delta]) ? $form_state['post']['field_title_' . $delta] : $field['title'],
        );
        $form['sphinx']['fields_wrapper'][$delta]['field_context_' . $delta] = array(
          '#type' => 'textfield',
          '#title' => t('Context field'),
          '#size' => 20,
          '#description' => t('Context field associated with this attr'),
          '#prefix' => '<div style="float:left; margin:1em">',
          '#suffix' => '</div>',
          '#default_value' => isset($form_state['post']['field_context_' . $delta]) ? $form_state['post']['field_context_' . $delta] : $field['context'],
        );
        $form['sphinx']['fields_wrapper'][$delta]['field_type_' . $delta] = array(
          '#type' => 'select',
          '#title' => t('Field type'),
          '#options' => $type_opts,
          '#prefix' => '<div style="float:left; margin:1em">',
          '#suffix' => '</div>',
          '#default_value' => isset($form_state['post']['field_type_' . $delta]) ? $form_state['post']['field_type_' . $delta] : $field['type'],
        );
        $form['sphinx']['fields_wrapper'][$delta]['field_renderer_' . $delta] = array(
          '#type' => 'select',
          '#options' => $renderers,
          '#title' => t('Attribute renderer'),
          '#prefix' => '<div style="float:left; margin:1em">',
          '#suffix' => '</div>',
          '#default_value' => isset($form_state['post']['field_renderer_' . $delta]) ? $form_state['post']['field_renderer_' . $delta] : $field['renderer'],
        );
      }
      $form['sphinx']['fields_wrapper']['fields_count'] = array(
        '#type' => 'hidden',
        '#value' => $fields_count,
      );

      $form['sphinx']['add_more_sphinx_field'] = array(
        '#type' => 'submit',
        '#value' => t('Add field'),
        '#submit' => array('poll_more_choices_submit'), // If no javascript action.
        '#ahah' => array(
          'path' => 'sphinx_advanced_indexes/js',
          'wrapper' => 'sphinx-fields-wrapper',
          'method' => 'replace',
          'effect' => 'fade',
        ),
      );

      $form['sphinx']['sql_attr_multi'] = array(
        '#type' => 'textarea',
        '#title' => t('Multiple attr description'),
        '#description' => t('Type the description for multiple attr'),
        '#default_value' => isset($form_state['post']['sql_attr_multi']) ? $form_state['post']['sql_attr_multi'] : $datasource->sql_attr_multi,
      );

      $form['sphinx']['sql_query_info'] = array(
        '#type' => 'textarea',
        '#title' => t('SQL Query Info'),
        '#description' => t('Type the SQL Query Info for this index'),
        '#required' => TRUE,
        '#default_value' => isset($form_state['post']['sql_query_info']) ? $form_state['post']['sql_query_info'] : $datasource->sql_query_info,
      );

      $form['sphinx']['sql_query_range'] = array(
        '#type' => 'textarea',
        '#title' => t('SQL Query Range'),
        '#description' => t('Type the SQL Query Range for this index'),
        '#required' => TRUE,
        '#default_value' => isset($form_state['post']['sql_query_range']) ? $form_state['post']['sql_query_range'] : $datasource->sql_query_range,
      );

      $form['sphinx']['sql_query_killlist'] = array(
        '#type' => 'textarea',
        '#title' => t('SQL Query Killlist'),
        '#description' => t('Type the SQL Query Killlist for main indexes only'),
        '#default_value' => isset($form_state['post']['sql_query_killlist']) ? $form_state['post']['sql_query_killlist'] : $datasource->sql_query_killlist,
      );

      $form['sphinx']['status'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable'),
        '#description' => t('Should this index be activated?'),
        '#default_value' => isset($form_state['post']['status']) ? $form_state['post']['status'] : $datasource->status,
      );
      $form['sphinx']['old_machine_name'] = array('#type' => 'hidden', '#value' => $index);
      $form['sphinx']['datasource'] = array('#type' => 'value', '#value' => $datasource);
      $form['sphinx']['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
      return $form;
    }
    else drupal_goto('admin/settings/sphinx_advanced/advanced_indexes');
  }
}

/**
 * Form validation handler for sphinx_advanced_index_edit().
 *
 * @see sphinx_advanced_index_edit_submit()
 */
function sphinx_advanced_index_edit_validate($form, &$form_values) {
  if (!preg_match('!^[a-z0-9_]+$!', $form_values['values']['machine_name'])) {
    form_set_error('machine_name', t('The machine-readable name must contain only lowercase letters, numbers, and underscores.'));
  }
  elseif ($form_values['values']['machine_name'] != $form_values['values']['old_machine_name'] && sphinx_advanced_existing_datasource($form_values['values']['machine_name'], TRUE)) {
    form_set_error('machine_name', t('The machine-readable name %machine_name is already taken.', array('%machine_name' => $form_values['values']['machine_name'])));
  }
}

/**
 * Form submission handler for sphinx_advanced_index_edit().
 *
 * @see sphinx_advanced_index_edit_validate()
 */
function sphinx_advanced_index_edit_submit($form_id, $form_values) {
  if ($form_values['values']) {
    // Transform fields list
    $fields = array();
    for ($i = 1; $i <= $form_values['values']['fields_count']; $i++) {
      $field_name = trim($form_values['values']['field_name_' . $i]);
      $field_type = trim($form_values['values']['field_type_' . $i]);
      $field_title = trim($form_values['values']['field_title_' . $i]);
      $field_renderer = trim($form_values['values']['field_renderer_' . $i]);
      $field_context = trim($form_values['values']['field_context_' . $i]);
      if ($field_name) $fields[$field_name] = array('name' => $field_name, 'title' => $field_title, 'type' => $field_type, 'renderer' => $field_renderer, 'context' => $field_context);
    }
    $form_values['values']['fields'] = serialize($fields);

    // Update datasource data
    $datasource = $form_values['values']['datasource'];
    foreach ($form_values['values'] as $key => $value) {
      if (!is_object($value)) $datasource->$key = $value;
    }
    $datasource->saveData();
  }
  drupal_goto('admin/settings/sphinx_advanced/advanced_indexes');
}

/**
 * Menu callback for AHAH additions.
 */
function sphinx_advanced_fields_js() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  // Get the form from the cache.
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  // We will run some of the submit handlers so we need to disable redirecting.
  $form['#redirect'] = FALSE;
  // We need to process the form, prepare for that by setting a few internals
  // variables.
  $form['#post'] = $_POST;
  $form['#programmed'] = FALSE;
  $form_state['post'] = $_POST;
  // Build, validate and if possible, submit the form.
  // drupal_process_form($form_id, $form, $form_state);
  // This call recreates the form relying solely on the form_state that the
  // drupal_process_form set up.
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  // Render the new output.
  $fields_form = $form['sphinx']['fields_wrapper'];
  unset($fields_form['#prefix'], $fields_form['#suffix']); // Prevent duplicate wrappers.
  //$output = theme('status_messages') . drupal_render($fields_form);
  $output = drupal_render($fields_form);
  drupal_json(array('status' => TRUE, 'data' => $output));
}
