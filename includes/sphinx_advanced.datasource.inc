<?php
// $Id$
/**
 * @file
 * Include for datasources for Sphinx integration with Drupal
 *
 */

class DatasourceSphinx {
  var $machine_name;
  var $fields = array();
  var $context_fields;
  var $parent_index;
  var $child_index;
  var $invalid = FALSE;

  /**
   * Get initial data for datasource.
   */
  function getData($include_disabled = FALSE) {
    $datasources = array();
    $sql = "SELECT s1.machine_name, s1.name, s1.status, s1.fields, s1.context_fields, s1.sql_query, s1.sql_query_info, s1.sql_query_range, s1.sql_attr_multi, s1.sql_query_killlist, s1.parent_index, s2.machine_name AS child_index, s1.last_reindex
            FROM {sphinx_advanced_indexes} AS s1
            LEFT JOIN {sphinx_advanced_indexes} AS s2 ON s2.parent_index = s1.machine_name AND s2.status = 1
            WHERE 1";
    // Search only for enabled indexes
    if (!$include_disabled) {
      $sql .= " AND s1.status = 1";
    }
    // Search only for given index
    if (isset($this->machine_name)) {
      $sql .= " AND s1.machine_name = '" . $this->machine_name . "'";
    }
    $query = db_query($sql);
    while ($item = db_fetch_object($query)) {
      $datasources[$item->machine_name] = $item;
    }

    // Process datasource data
    if (isset($this->machine_name) && !empty($datasources[$this->machine_name])) {
      foreach ($datasources[$this->machine_name] as $field_name => $field) {
        if ($field_name == 'fields') $this->$field_name = unserialize($field);
        elseif ($field_name == 'context_fields') $this->$field_name = explode(',', $field);
        else $this->$field_name = $field;
      }
    }
    elseif (!empty($datasources)) {
      $this->list = $datasources;
    }
    else {
      $this->invalid = TRUE;
    }
  }

  /**
   * Datasource saver.
   */
  function saveData() {
    if (isset($this->machine_name)) {
      // Transform object data into array
      $data = (array)$this;
      if ($this->isExists()) {
        // Update datasource data in DB
        db_query("UPDATE {sphinx_advanced_indexes} SET machine_name = '%s', name = '%s', sql_query = '%s', fields = '%s', context_fields = '%s', sql_query_info = '%s',  sql_query_range ='%s', status = %d, sql_attr_multi = '%s', sql_query_killlist = '%s', parent_index = '%s' WHERE machine_name = '%s';",
          $data['machine_name'],
          $data['name'],
          $data['sql_query'],
          $data['fields'],
          $data['context_fields'],
          $data['sql_query_info'],
          $data['sql_query_range'],
          $data['status'],
          $data['sql_attr_multi'],
          $data['sql_query_killlist'],
          $data['parent_index'],
          $data['old_machine_name']
        );
      }
      else {
        // Insert datasource data to DB
        drupal_write_record('sphinx_advanced_indexes', $data);
      }
    }
  }

  /**
   * Datasource destructor.
   */
  function deleteData() {
    if (isset($this->machine_name)) {
      // Delete datasource from DB
      db_query('DELETE FROM {sphinx_advanced_indexes} WHERE machine_name = "%s"', $this->machine_name);
    }
  }

  /**
   * Datasource full loader.
   */
  function loadData() {
    if (isset($this->machine_name)) {
      // Load all datasource data from DB
      $result = db_query('SELECT * FROM {sphinx_advanced_indexes} WHERE machine_name = "%s"', $this->machine_name);
      $index_data = db_fetch_object($result);
      // Gather datasource data into object vars
      foreach ($index_data as $key => $value) {
        // Transform index attribute fields
        if ($key == 'fields') {
          $this->fields = unserialize($index_data->fields);
        }
        else {
          $this->$key = $value;
        }
      }
    }
  }

  /**
   * Check if this datasource already exists in DB.
   */
  function isExists() {
    if (isset($this->old_machine_name)) {
      $result = db_result(db_query('SELECT machine_name FROM {sphinx_advanced_indexes} WHERE machine_name = "%s"', $this->old_machine_name));
      if ($result) return TRUE;
    }
    elseif (isset($this->machine_name)) {
      $result = db_result(db_query('SELECT machine_name FROM {sphinx_advanced_indexes} WHERE machine_name = "%s"', $this->machine_name));
      if ($result) return TRUE;
    }
  }
}
